package com.cerberus;

import static spark.Spark.ipAddress;
import static spark.Spark.get;
import static spark.Spark.port;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.udger.parser.UdgerIpResult;
import org.udger.parser.UdgerParser;
import org.udger.parser.UdgerUaResult;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UAServer {
	private final static Logger logger = LoggerFactory.getLogger(UAServer.class);
	private static String udgerFile;

	static {
		logger.info("=========================Starting UA Parser server=========================");
		/*System.out.printf("file=%s\n", System.getProperty("file"));
		System.out.printf("host=%s\n", System.getProperty("host"));
		System.out.printf("port=%s\n", System.getProperty("port"));*/
		
		udgerFile = System.getProperty("file") != null ? System.getProperty("file") : "C:/Users/rpramo/Downloads/udgerdb_v3.dat";
		String host = System.getProperty("host") != null ? System.getProperty("host") : "localhost";
		String port = System.getProperty("port") != null ? System.getProperty("port") : "4567";

		ipAddress(host);
		port(Integer.parseInt(port));
		logger.info("Using path={}, host={}, port={}", udgerFile, host, port);
	}

	public static void main(String[] args) {
		UdgerParser up = new UdgerParser(udgerFile);
		try {
			up.prepare();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Unable to start the UA parser, error: ", e);
			System.exit(1);
		}

		get("/ua", (req, res) -> {
			UdgerUaResult uaRet = up.parseUa(req.queryParams("uas"));
			ObjectMapper mapper = new ObjectMapper();
			String uaJson = mapper.writeValueAsString(uaRet);
			res.type("application/json");
			// res.body(uaJson);
			return uaJson;
			// return res;
			// return jsonInString;
		});

		get("/ip", (req, res) -> {
			UdgerIpResult ipRet = up.parseIp(req.queryParams("ipaddr"));
			ObjectMapper mapper = new ObjectMapper();
			String uaJson = mapper.writeValueAsString(ipRet);
			res.type("application/json");
			return uaJson;
		});
	}

}