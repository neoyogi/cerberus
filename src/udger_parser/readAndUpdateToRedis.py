import sqlite3
import redis
import os
import socket
import struct
import argparse
import re


def ip2long(ip):
    """
    Convert an IP string to long
    """
    packedIP = socket.inet_aton(ip)
    return struct.unpack("!L", packedIP)[0]

def convert_list_to_pipe_sep_string(listOfValues):
    returnlist = []
    for i in list(listOfValues):
        i = map(str, i)
        returnlist.append("||".join(i))
    return returnlist

class ReadFromSqlite(object):
    def __init__(self, sqlitefile):
        if os.path.exists(sqlitefile):
            self.sqlitefile = sqlitefile
        else:
            raise "check the file path again"

    def get_crawler_sql_string(self, data_type):
        crawler_sql_fields = "name, ip, ip_hostname, ip_country, ip_city, ip_country_code, family_code, family_homepage, vendor_code, vendor_homepage "
        crawler_type = {
            "uncategorised":0,
            "search_engine_bot":1,
            "site_monitor":2,
            "screenshot_creator":3,
            "link_checker":5,
            "web_scraper":10,
            "vulnerability_scanner":20,
            "virus_scanner":21,
            "speed_tester":25,
            "feed_fetcher":30,
            "tool":50,
            "marketing":70,
            "unrecognized":500
        }

        crawler_sql_statement = """select {} from udger_ip_list as a, udger_crawler_list b, udger_crawler_class c where a.crawler_id = b.id and b.class_id = c.id and a.class_id = 99 and c.id = {} """.format(crawler_sql_fields, crawler_type[data_type])
        return crawler_sql_statement

    def get_udger_ip_sql(self, ip_type):
        sql_fields = " ip, ip_hostname, ip_country, ip_city, ip_country_code "
        ip_type_fields = {
            "tor_exit_node":1,
            "fake_crawler":2,
            "known_attack_source":3,
            "cgi_proxy":4,
            "vpn_service":5,
            "web_proxy":6,
            "web_scraper":7,
            "known_attack_source_ssh":10,
            "known_attack_source_mail":11,
            "crawler":99,
            "unrecognized":100
        }
        sql_statement = """select {} from udger_ip_list where class_id={}""".format(sql_fields, ip_type_fields[ip_type])
        return sql_statement

    def getDataCenterIPs(self):
        try:
            con = sqlite3.connect(self.sqlitefile)
            cursor = con.cursor()
            cursor.execute("""
            SELECT
            id,
            homepage AS datacenter_homepage,
            name_code AS datacenter_name_code,
            name AS datacenter_name,
                ip_from,
                ip_to,
                iplong_from,
                iplong_to
                FROM
                    udger_datacenter_range
                JOIN
                    udger_datacenter_list ON udger_datacenter_range.datacenter_id = udger_datacenter_list.id

            """)

            return(cursor.fetchall())

        except sqlite3.Error as e:
            print(e)

        finally:
            con.close()


    def getCrawlers(self, crawler_type):
        try:
            con = sqlite3.connect(self.sqlitefile)
            cursor = con.cursor()
            cursor.execute(self.get_crawler_sql_string(crawler_type))
            return(cursor.fetchall())
        except sqlite3.Error as e:
            print(e)
        finally:
            con.close()

    def get_bot_ips(self, ip_type):
        try:
            con = sqlite3.connect(self.sqlitefile)
            cursor = con.cursor()
            sql_statement = self.get_udger_ip_sql(ip_type)
            cursor.execute(sql_statement)
            # cursor.execute("select ip, ip_hostname, ip_country, ip_city, ip_country_code from udger_ip_list where class_id=7")
            return(cursor.fetchall())
        except sqlite3.Error as e:
            print(e)
        finally:
            con.close()

    def get_list(self, sql_statement):
        try:
            con = sqlite3.connect(self.sqlitefile)
            cursor = con.cursor()
            cursor.execute(sql_statement)
            return cursor.fetchall()
        except sqlite3.Error as e:
            print(e)
        finally:
            con.close()

class UpdateRedis(object):
    def __init__(self, redis_ip, redis_port):
        self.pool = redis.ConnectionPool(host=redis_ip, port=int(redis_port), db=0)

    def del_existing_redis_keys(self, keyname):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.delete(keyname)
            if response:
                return response
        except redis.RedisError as e:
            print(e)
            return
        finally:
            del redisCon

    def getRedis(self, get_query):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.get(get_query)
            if response:
                return response
        except redis.RedisError as e:
            print(e)
            return
        finally:
            del redisCon

    def setRedis(self, name, set_data):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.set(name=name, value=set_data)
            if response:
                return response

        except redis.RedisError as e:
            print(e)
        finally:
            del redisCon

    def hmSetRedis(self, name, hmap):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.hmset(name=name, mapping=hmap)
            if response:
                return response
        except redis.RedisError as e:
            print(e)
        finally:
            del redisCon

    def saddRedis(self, name, values):
         try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.sadd(name, *values)
            if response:
                return response
         except redis.RedisError as e:
            print(e)
         finally:
            del redisCon

    def zaddRedis(self, name, value, longip):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.zadd(name, value, int(longip))
            if response:
                return response
        except redis.RedisError as e:
            print(e)
        finally:
            del redisCon


    def getIprange(self, name, min="-inf", max="+inf"):
        try:
            redisCon = redis.Redis(connection_pool=self.pool)
            response = redisCon.zrangebyscore(name=name, min=min, max=max)
            # print(response)
            return response
        except redis.RedisError as e:
            print(e)
        finally:
            del redisCon

    def set_datacenter_info_to_redis(self, rows):
        if rows:
            for row in rows:
                id, datacenter_homepage, datacenter_name_code, datacenter_name, ip_from, ip_to, iplong_from, iplong_to = row
                delimited_data = str(id) + "||" + datacenter_homepage + "||" + datacenter_name_code + "||" + datacenter_name + "||" + ip_from + "||" + ip_to + "||" + str(iplong_from) +"||" + str(iplong_to)
                self.zaddRedis(name="ip_datacenter", longip=iplong_to, value= str(delimited_data))

    def check_data_center_ip_range(self, ip_to_lookup):
        try:
            ip_to_lookup_int = ip2long(ip_to_lookup)
            upper_ranges = update_redis_obj.getIprange(name="datacenter_ips_upper", min=ip_to_lookup_int)
            for range in upper_ranges:
                data = (str(range).split("||"))
                lower_range = int(data[-2])
                if ip_to_lookup_int >= lower_range:
                    print(range)
        except Exception as e:
            print("error has occured: message:", e)

    def set_crawler_info(self, crawlers, redis_crawler_name):
        try:
            # self.del_existing_redis_keys(redis_crawler_name)
            for crawler in crawlers:
                name, ip, ip_hostname, ip_country, ip_city, ip_country_code, family_code, family_homepage, vendor_code, vendor_homepage = crawler
                try:
                    crawler_longint_ip = ip2long(ip)
                except socket.error:
                    continue
                value = str.strip(name)+ "||" + ip + "||" + str.strip(ip_hostname) + "||" + str.strip(ip_country) + "||" + str.strip(ip_city) + "||" + str.strip(ip_country_code) + "||" + str.strip(family_code) + "||" + str.strip(family_homepage) + "||" + str.strip(vendor_code) + "||" + str.strip(vendor_homepage)
                self.zaddRedis(redis_crawler_name, value, crawler_longint_ip)
        except Exception as e:
            print(e)

    def set_bot_info_redis(self, scrapers, name):
        count = 0
        try:
            # self.del_existing_redis_keys(name)
            for scraper in scrapers:
                ip, ip_hostname, ip_country, ip_city, ip_country_code = scraper
                try:
                    ip_longint = ip2long(str.strip(ip))
                    count = count +1
                except socket.error:
                    continue
                value = str.strip(ip) + "||" + str.strip(ip_country) + "||" + str.strip(ip_city) + "||" + str.strip(ip_country_code)
                self.zaddRedis(name, value, ip_longint )
            print(count)
        except Exception as e:
            print(e)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="argument parser for updating information on master redis")
    parser.add_argument("-d", "--dat", help="Udger data file", required=True, default="/home/yogesh/udger/udgerdb_v3.dat")
    parser.add_argument("-ip", "--ip", help="Redis IP address", required=True, default="127.0.0.1")
    parser.add_argument("-p", "--port", help="Redis port number", required=True, default=7778)
    args = vars(parser.parse_args())

    update_redis_obj = UpdateRedis(redis_ip=args["ip"], redis_port=args["port"])

    sqldata = ReadFromSqlite(args["dat"])

    update_redis_obj.del_existing_redis_keys("ip_crawler_marketing")
    update_redis_obj.del_existing_redis_keys("ip_bot")
    update_redis_obj.del_existing_redis_keys("ip_crawler_uncategorised")
    update_redis_obj.del_existing_redis_keys("ip_crawler_search_engine_bot")
    update_redis_obj.del_existing_redis_keys("ip_crawler_scanner")
    update_redis_obj.del_existing_redis_keys("ip_datacenter")
    update_redis_obj.del_existing_redis_keys("ip_proxy")
    update_redis_obj.del_existing_redis_keys("ip_crawler_search_engine_bot")
    update_redis_obj.del_existing_redis_keys("ip_scraper")
    update_redis_obj.del_existing_redis_keys("ua_client_regex")
    update_redis_obj.del_existing_redis_keys("ua_deviceclass_regex")
    update_redis_obj.del_existing_redis_keys("ua_os_regex")
    update_redis_obj.del_existing_redis_keys("ua_client_list")
    update_redis_obj.del_existing_redis_keys("ua_os_list")
    update_redis_obj.del_existing_redis_keys("ua_deviceclass_list")
    update_redis_obj.del_existing_redis_keys("ua_client_class")
    update_redis_obj.del_existing_redis_keys("ua_client_os_relation")

    rows = sqldata.getDataCenterIPs()
    update_redis_obj.set_datacenter_info_to_redis(rows)

    search_engine_crawlers_list = sqldata.getCrawlers("search_engine_bot")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_search_engine_bot")

    search_engine_crawlers_list = sqldata.getCrawlers("site_monitor")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("screenshot_creator")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_search_engine_bot")

    search_engine_crawlers_list = sqldata.getCrawlers("link_checker")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("web_scraper")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("vulnerability_scanner")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("virus_scanner")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("speed_tester")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_scanner")

    search_engine_crawlers_list = sqldata.getCrawlers("feed_fetcher")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_marketing")

    search_engine_crawlers_list = sqldata.getCrawlers("uncategorised")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_uncategorised")

    search_engine_crawlers_list = sqldata.getCrawlers("marketing")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_marketing")

    search_engine_crawlers_list = sqldata.getCrawlers("tool")
    update_redis_obj.set_crawler_info(search_engine_crawlers_list, "ip_crawler_marketing")

    bot_list= sqldata.get_bot_ips("tor_exit_node")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_proxy")

    bot_list = sqldata.get_bot_ips("fake_crawler")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_scraper")

    bot_list = sqldata.get_bot_ips("known_attack_source")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_bot")

    bot_list = sqldata.get_bot_ips("cgi_proxy")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_proxy")

    bot_list = sqldata.get_bot_ips("vpn_service")
    print(len(bot_list))
    update_redis_obj.set_bot_info_redis(bot_list, "ip_proxy")

    bot_list = sqldata.get_bot_ips("web_proxy")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_proxy")

    bot_list = sqldata.get_bot_ips("web_scraper")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_scraper")

    bot_list = sqldata.get_bot_ips("known_attack_source_ssh")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_bot")

    bot_list = sqldata.get_bot_ips("known_attack_source_mail")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_bot")

    bot_list = sqldata.get_bot_ips("unrecognized")
    update_redis_obj.set_bot_info_redis(bot_list, "ip_scraper")

    ################# UA Parsing ####################

    udger_client_regex_sql = """SELECT client_id, regstring FROM udger_client_regex ORDER by sequence ASC"""
    udger_client_regex = sqldata.get_list(udger_client_regex_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_client_regex)
    update_redis_obj.saddRedis("ua_client_regex",redis_data)

    udger_deviceclass_regex_sql = """SELECT deviceclass_id, regstring FROM udger_deviceclass_regex ORDER by sequence ASC"""
    udger_deviceclass_regex_list = sqldata.get_list(udger_deviceclass_regex_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_deviceclass_regex_list)
    update_redis_obj.saddRedis("ua_deviceclass_regex",redis_data)


    udger_os_regex_sql = """SELECT os_id, regstring FROM udger_os_regex ORDER by sequence ASC"""
    udger_os_regex_list = sqldata.get_list(udger_os_regex_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_os_regex_list)
    update_redis_obj.saddRedis("ua_os_regex", redis_data)

    udger_client_list_sql = """SELECT id, class_id, name,engine,vendor,icon FROM udger_client_list"""
    udger_client_list = sqldata.get_list(udger_client_list_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_client_list)
    update_redis_obj.saddRedis("ua_client_list", redis_data)

    udger_os_list_sql = """SELECT id, name, family, vendor, icon FROM udger_os_list"""
    udger_os_list = sqldata.get_list(udger_os_list_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_os_list)
    update_redis_obj.saddRedis("ua_os_list", redis_data)

    udger_deviceclass_list_sql = "SELECT id, name, icon FROM udger_deviceclass_list"
    udger_deviceclass_list = sqldata.get_list(udger_deviceclass_list_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_deviceclass_list)
    update_redis_obj.saddRedis("ua_deviceclass_list", redis_data)

    udger_client_class_sql = "SELECT id, client_classification FROM udger_client_class"
    udger_client_class_list = sqldata.get_list(udger_client_class_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_client_class_list)
    update_redis_obj.saddRedis("ua_client_class", redis_data)

    udger_client_os_relation_sql = "SELECT client_id, os_id FROM udger_client_os_relation"
    udger_client_os_relation_list = sqldata.get_list(udger_client_os_relation_sql)
    redis_data = convert_list_to_pipe_sep_string(udger_client_os_relation_list)
    update_redis_obj.saddRedis("ua_client_os_relation", redis_data)
