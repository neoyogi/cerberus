CREATE TABLE udger_db_info (
    "key" INTEGER,
    "version" TEXT,
    "information" TEXT,
    "lastupdate" INTEGER
);
CREATE TABLE "udger_client_class" (
    "id" INTEGER,
    "client_classification" TEXT,
    "client_classification_code" TEXT,
    "deviceclass_id" INTEGER
);
CREATE TABLE "udger_client_list" (
    "id" INTEGER,
    "class_id"  INTEGER,
    "name" TEXT,
	"name_code" TEXT,
	"homepage" TEXT,
	"icon" TEXT,
	"icon_big" TEXT,
	"engine" TEXT,
	"vendor" TEXT,
	"vendor_code" TEXT,
	"vendor_homepage" TEXT,
	"uptodate_current_version" TEXT
);
CREATE TABLE "udger_client_os_relation" (
    "client_id" INTEGER,
    "os_id"  INTEGER
);
CREATE TABLE "udger_deviceclass_list" (
    "id" INTEGER,
    "name" TEXT,
	"name_code" TEXT,
	"icon" TEXT,
	"icon_big" TEXT
);
CREATE TABLE "udger_fragment_regex" (
    "regstring1"  TEXT,
"regstring2"  TEXT,
"regstring3"  TEXT,
"regstring4"  TEXT,
"description"  TEXT,
    "sequence" INTEGER
);
CREATE TABLE "udger_os_list" (
    "id" INTEGER,
    "name"  TEXT,
    "name_code" TEXT,
    "homepage" TEXT,
    "family" TEXT,
    "family_code" TEXT,
    "icon" TEXT,
    "icon_big" TEXT,
    "vendor" TEXT,
    "vendor_code" TEXT,
    "vendor_homepage" TEXT
);
CREATE INDEX "keyIdx" on udger_db_info (key ASC);
CREATE TABLE "udger_client_regex_words" (
    "id" INTEGER,
    "word" TEXT,
    "count" INTEGER
);
CREATE TABLE "udger_deviceclass_regex_words" (
    "id" INTEGER,
    "word" TEXT,
    "count" INTEGER
);
CREATE TABLE "udger_os_regex_words" (
    "id" INTEGER,
    "word" TEXT,
    "count" INTEGER
);
CREATE TABLE udger_client_regex (
    "client_id" INTEGER,
    "regstring" TEXT,
    "sequence" INTEGER,
    "word_id" INTEGER
, "word2_id" INTEGER);
CREATE TABLE udger_deviceclass_regex (
    "deviceclass_id" INTEGER,
    "regstring" TEXT,
    "sequence" INTEGER,
    "word_id" INTEGER
, "word2_id" INTEGER);
CREATE TABLE udger_os_regex (
    "os_id" INTEGER,
    "regstring" TEXT,
    "sequence" INTEGER,
    "word_id" INTEGER
, "word2_id" INTEGER);
CREATE TABLE "udger_devicename_list" (
    "regex_id" INTEGER,
    "brand_id" INTEGER,
    "code" TEXT,
    "marketname" TEXT
);
CREATE TABLE udger_devicename_regex (
    "id" INTEGER,
    "os_family_code" TEXT,
    "os_code" TEXT,
    "regstring" TEXT,
    "sequence" INTEGER
);
CREATE TABLE udger_devicename_brand (
    "id" INTEGER,
    "brand_code" TEXT,
    "brand_url" TEXT,
    "icon" TEXT,
    "icon_big" TEXT
, "brand" TEXT);
CREATE INDEX regexIdx on udger_devicename_list (regex_id);
CREATE INDEX codeIdx on udger_devicename_list (code);
CREATE INDEX brandIdx on udger_devicename_list (brand_id);
CREATE INDEX brandIdIdx on udger_devicename_brand (id);
CREATE UNIQUE INDEX idclassIdx on udger_client_class (id ASC);
CREATE INDEX seqClientIdx on udger_client_regex (sequence ASC);
CREATE INDEX seqOsIdx on udger_os_regex (sequence ASC);
CREATE INDEX seqDeviceIdx on udger_deviceclass_regex (sequence ASC);
CREATE INDEX seqFragIdx on udger_fragment_regex (sequence ASC);
CREATE INDEX idxClientRegexIdx ON udger_client_regex (client_id);
CREATE UNIQUE INDEX idxClientListId ON udger_client_list(id);
CREATE INDEX idxClientListClassId ON udger_client_list(class_id);
CREATE INDEX idxWord1 ON udger_client_regex(word_id);
CREATE INDEX idxOsRegexIdx ON udger_os_regex(os_id);
CREATE INDEX idxClientOsRelation2OsIdx ON udger_client_os_relation(os_id);
CREATE UNIQUE INDEX os_idx ON udger_os_list(id);
CREATE INDEX idxDeviceClassRegexIdx ON udger_deviceclass_regex(deviceclass_id);
CREATE UNIQUE INDEX deviceclass_idx ON udger_deviceclass_list(id);
CREATE INDEX idxClientClass2DeviceClassIdx on udger_client_class(deviceclass_id);
