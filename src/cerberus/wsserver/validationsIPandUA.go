package main

import (
	"encoding/binary"
	"errors"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"net"
	"strconv"
	"strings"
	"sync"
)

func ip2int(ip string) uint32 {
	parsed_ip := net.ParseIP(ip)
	if len(parsed_ip) == 16 {
		return binary.BigEndian.Uint32(parsed_ip[12:16])
	}
	return binary.BigEndian.Uint32(parsed_ip)
}

func (is *ClientInfo) ipValidationWorker() {
	defer is.defaultWG.Done()
	log := is.log.WithFields(logrus.Fields{"func": "ipValidationWorker"})
	defer log.Debugln("Exit worker")

	for {
		select {
		case requestdata := <-is.ipCheckCh:
			if requestdata.BlockType != "" {
				continue
			}
			func() {
				defer requestdata.checkDBWG.Done()
				defer log.Debugln("checking of the IP address done!!")
				is.checkRedisCacheForIp(requestdata)
				if requestdata.IpParsed != nil {
					is.validateIPaddress(requestdata)
					is.setIPtoRedisCache(requestdata)
				}
			}()

		case <-is.doneCh:
			return
		}
	}
}

func (is *ClientInfo) validateIPaddress(requestdata *RequestData) error {
	ipdata := new(IPdata)
	log := is.log.WithFields(logrus.Fields{"func": "validateIPaddress"})
	defer log.Debugln("Exit func")
	ip := ip2int(requestdata.Ip)
	validateIpWG := new(sync.WaitGroup)
	returnerrors := []error{}
	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_datacenter"})
		defer log.Debugln("done")
		datacenter_ip_info, err := getZrangeByScore("ip_datacenter", ip, "+INF", 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on localredis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(datacenter_ip_info) > 0 {
			redis_data := strings.Split(datacenter_ip_info[0], "||")
			if len(redis_data) >= 8 {
				upperLimitIp, err := strconv.ParseInt(redis_data[7], 10, 64)
				if err != nil {
					log.Warnf("failed to convert from string to int32, error: %s Data: %#v", err, redis_data)
				}
				if int64(ip) <= upperLimitIp {
					ipdata.IsIpDataCenter = true
					ipdata.IpVendor = redis_data[2]
					ipdata.IpVendorUrl = redis_data[1]
				}
			} else {
				log.Warnln("length of redis_data less than 8, redis_data:", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine bot_ip"})
		defer log.Debugln("done")
		botIPInfo, err := getZrangeByScore("ip_bot", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(botIPInfo) > 0 {
			redis_data := strings.Split(botIPInfo[0], "||")
			if len(redis_data) >= 3 {
				ipdata.IsIpBot = true
				ipdata.IpBotCountry = redis_data[1]
				ipdata.IpBotCity = redis_data[2]
			} else {
				log.Warnln("len of redis_data less than 3, redis_data: ", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_crawler_marketing"})
		defer log.Debugln("done")
		crawler_info, err := getZrangeByScore("ip_crawler_marketing", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(crawler_info) > 0 {
			redis_data := strings.Split(crawler_info[0], "||")
			if len(redis_data) >= 8 {
				ipdata.IsIpGoodCrawler = true
				ipdata.IpHostname = redis_data[2]
				ipdata.IpBotCountry = redis_data[3]
				ipdata.IpBotCity = redis_data[4]
				ipdata.IpVendor = redis_data[6]
				ipdata.IpVendorUrl = redis_data[7]
				ipdata.IpCrawlerCategory = "marketing"
			} else {
				log.Warnln("length of redis_data is less than 8, redis data:", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_crawler_search_engine_bot"})
		defer log.Debugln("done")
		crawler_info_search_engine, err := getZrangeByScore("ip_crawler_search_engine_bot", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(crawler_info_search_engine) > 0 {
			redis_data := strings.Split(crawler_info_search_engine[0], "||")
			if len(redis_data) >= 8 {
				log.Debugln(redis_data)
				ipdata.IpCrawlerCategory = "search_engine"
				ipdata.IsIpGoodCrawler = true
				ipdata.IpHostname = redis_data[2]
				ipdata.IpBotCountry = redis_data[3]
				ipdata.IpBotCity = redis_data[4]
				ipdata.IpVendor = redis_data[6]
				ipdata.IpVendorUrl = redis_data[7]
			} else {
				log.Warnln("invalid data in redis_data split, data: ", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_crawler_uncategorised"})
		defer log.Debugln("done")
		crawler_uncategorised, err := getZrangeByScore("ip_crawler_uncategorised", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(crawler_uncategorised) > 0 {
			redis_data := strings.Split(crawler_uncategorised[0], "||")
			if len(redis_data) >= 8 {
				ipdata.IpCrawlerCategory = "uncategorised"
				ipdata.IpHostname = redis_data[2]
				ipdata.IpBotCountry = redis_data[3]
				ipdata.IpBotCity = redis_data[4]
				ipdata.IpVendor = redis_data[6]
				ipdata.IpVendorUrl = redis_data[7]
			} else {
				log.Warnln("length of redis_data less than 8, redis_data: ", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_scraper"})
		defer log.Debugln("done")
		ip_scraper, err := getZrangeByScore("ip_scraper", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(ip_scraper) > 0 {
			redis_data := strings.Split(ip_scraper[0], "||")
			if len(redis_data) > 3 {
				ipdata.IsIpBot = true
				ipdata.IpBotCountry = redis_data[1]
				ipdata.IpBotCity = redis_data[2]
			} else {
				log.Warnln("length of redis_data less than 3, redis_data: ", redis_data)
			}

		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_crawler_scanner"})
		defer log.Debugln("done")
		crawler_scanner, err := getZrangeByScore("ip_crawler_scanner", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(crawler_scanner) > 0 {
			redis_data := strings.Split(crawler_scanner[0], "||")
			if len(redis_data) >= 8 {
				ipdata.IsIpGoodCrawler = true
				ipdata.IpCrawlerCategory = "scanner"
				ipdata.IpHostname = redis_data[2]
				ipdata.IpBotCountry = redis_data[3]
				ipdata.IpBotCity = redis_data[4]
				ipdata.IpVendor = redis_data[6]
				ipdata.IpVendorUrl = redis_data[7]
			} else {
				log.Warnln("length of redis_data less than 8, data: ", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_tor_exit_node"})
		defer log.Debugln("done")
		tor_nodes, err := getZrangeByScore("ip_tor_exit_node", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(tor_nodes) > 0 {
			redis_data := strings.Split(tor_nodes[0], "||")
			if len(redis_data) >= 3 {
				ipdata.IsIpTor = true
				ipdata.IpBotCountry = redis_data[1]
				ipdata.IpBotCity = redis_data[2]
			} else {
				log.Warnln("Length of redis_data is 3, data:", redis_data)
			}
		}
	}()

	validateIpWG.Add(1)
	//go func() {
	func() {
		defer validateIpWG.Done()
		log := is.log.WithFields(logrus.Fields{"func": "subroutine ip_proxy"})
		defer log.Debugln("done")
		vpn_ip, err := getZrangeByScore("ip_proxy", ip, ip, 0, 1)
		if err != nil {
			log.Warnln("Failed to execute zrangebyscore on local redis")
			returnerrors = append(returnerrors, err)
			return
		}
		if len(vpn_ip) > 0 {
			redis_data := strings.Split(vpn_ip[0], "||")
			if len(redis_data) >= 3 {
				ipdata.IsIpProxy = true
				ipdata.IpBotCountry = redis_data[1]
				ipdata.IpBotCity = redis_data[2]
			} else {
				log.Warnln("len of redis_data < 3, data: ", redis_data)
			}
		}
	}()
	validateIpWG.Wait()
	requestdata.IpParsed = ipdata
	if len(returnerrors) > 0 {
		for err := range returnerrors {
			log.Warnln("error while check IP address: ", err)
		}
		return errors.New("redis IP check errors")
	}
	return nil
}
func (is *ClientInfo) setIPtoRedisCache(requestdata *RequestData) {
	log := is.log.WithFields(logrus.Fields{"func": "setIPtoRedisCache"})
	defer log.Debugln("Exit func")
	redisConpool := masterRedisPool[is.cid]
	redisCon := redisConpool.Get()
	defer redisCon.Close()
	log.Debugln("setting IP to Master Redis cache, IP = ", requestdata.Ip)
	dataList := make([]string, 0)
	if requestdata.IpParsed.IsIpTor {
		dataList = append(dataList, "IsIpTor", "true", "IpBotCountry", requestdata.IpParsed.IpBotCountry, "IpBotCity", requestdata.IpParsed.IpBotCity)
	}
	_, err := redisCon.Do("hmset", "cache_ip:"+requestdata.Ip, "IsIpTor", requestdata.IpParsed.IsIpTor, "IpBotCountry", requestdata.IpParsed.IpBotCountry, "BotCity", requestdata.IpParsed.IpBotCity, "IpCrawlerCategory", requestdata.IpParsed.IpCrawlerCategory, "IpHostname", requestdata.IpParsed.IpHostname, "IpVendor", requestdata.IpParsed.IpVendor, "IpVendorUrl", requestdata.IpParsed.IpVendorUrl, "IsIpBot", requestdata.IpParsed.IsIpBot, "IsIpDataCenter", requestdata.IpParsed.IsIpDataCenter, "IsIpGoodCrawler", requestdata.IpParsed.IsIpGoodCrawler, "IsIpProxy", requestdata.IpParsed.IsIpProxy)
	if err != nil {
		log.Warnln("Failed to set the IP on master redis, error: ", err)
		return
	}
	_, err = redisCon.Do("expire", "cache_ip:"+requestdata.Ip, "86400")
	if err != nil {
		log.Warnln("failed to set the expire key, error: ", err)
	}
}

func (is *ClientInfo) checkRedisCacheForIp(requestdata *RequestData) {
	log := is.log.WithFields(logrus.Fields{"func": "checkRedisCacheForIp"})
	localredisCon := localRedisPool.Get()
	key := "cache_ip:" + requestdata.Ip
	defer localredisCon.Close()
	check_ip_in_cache, err := redis.Int(localredisCon.Do("exists", key))
	if err != nil {
		log.Warnln("Failed to perform exists command on local redis, error: ", err)
		is.once.Do(is.closeClient)
		return
	}
	if check_ip_in_cache == 1 {
		ipdata := new(IPdata)
		log.Debugf("Ip is available in cache, IP: %s ", requestdata.Ip)
		log.Warnln("ip found in cache")
		if IpBotCity := hgetRediskeyString(key, "BotCity"); IpBotCity != "" {
			ipdata.IpBotCity = IpBotCity
		}
		if IsIpTor := hgetRediskeyInt(key, "IsIpTor"); IsIpTor != 0 {
			ipdata.IsIpTor = true
		}
		if IpBotCountry := hgetRediskeyString(key, "IpBotCountry"); IpBotCountry != "" {
			ipdata.IpBotCountry = IpBotCountry
		}
		if IsIpGoodCrawler := hgetRediskeyInt(key, "IsIpGoodCrawler"); IsIpGoodCrawler != 0 {
			ipdata.IsIpGoodCrawler = true
		}
		if IsIpBot := hgetRediskeyInt(key, "IsIpBot"); IsIpBot != 0 {
			ipdata.IsIpBot = true
		}
		if IpCrawlerCategory := hgetRediskeyString(key, "IpCrawlerCategory"); IpCrawlerCategory != "" {
			ipdata.IpCrawlerCategory = IpCrawlerCategory
		}
		if IsIpDataCenter := hgetRediskeyInt(key, "IsIpDataCenter"); IsIpDataCenter != 0 {
			ipdata.IsIpDataCenter = true
		}
		if IpVendor := hgetRediskeyString(key, "IpVendor"); IpVendor != "" {
			ipdata.IpVendor = IpVendor
		}
		if IsIpProxy := hgetRediskeyInt(key, "IsIpProxy"); IsIpProxy != 0 {
			ipdata.IsIpProxy = true
		}
		if IpHostname := hgetRediskeyString(key, "IpHostname"); IpHostname != "" {
			ipdata.IpHostname = IpHostname
		}
		if IpVendorUrl := hgetRediskeyString(key, "IpVendorUrl"); IpVendorUrl != "" {
			ipdata.IpVendorUrl = IpVendorUrl
		}
		requestdata.IpParsed = ipdata
	}
}
