package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
)

func (is *ClientInfo) decisionWorker() {
	defer is.defaultWG.Done()
	log := is.log.WithFields(logrus.Fields{"func": "decisionworker"})
	defer log.Debugln("Exit worker")

	for {
		select {
		case requestdata := <-is.validationCh:
			if requestdata.BlockType != "" {
				continue
			}
			requestdata.checkDBWG.Wait()
			func() {
				defer requestdata.checkWG.Done()
				is.decisionMaker(requestdata)
			}()
		case <-is.doneCh:
			return
		}
	}
}

func (is *ClientInfo) decisionMaker(requestdata *RequestData) error {
	log := is.log.WithFields(logrus.Fields{"func": "decisionMaker"})
	defer log.Debugln("Exit")
	if requestdata.countIPThreshold1 > is.thresholdCount1 || requestdata.countIPThreshold2 > is.thresholdCount2 {
		log.Debugln("threshold crossed, blocking")
		err := is.blocking(requestdata)
		return err
	} else if requestdata.IpParsed != nil && requestdata.IpParsed.IsIpBot {
		log.Debugln("It's an Bot IP, blocking")
		err := is.blocking(requestdata)
		return err
	}
	return nil
}

func (is *ClientInfo) blocking(requestData *RequestData) error {
	log := is.log.WithFields(logrus.Fields{"func": "blocking"})
	defer log.Debugln("exit")
	localredisCon := localRedisPool.Get()
	defer localredisCon.Close()
	key := "blocked:cid:" + is.cid + ":" + requestData.Ip + ":" + requestData.Ua
	checkRedisForKey, err := redis.Int(localredisCon.Do("exists", key))
	if err != nil {
		log.Warnln("Failed to perform query to local redis, error: ", err)
		is.once.Do(is.closeClient)
		return err
	}
	if checkRedisForKey == 0 {
		masterRedisCon := is.masterRedisPool.Get()
		defer masterRedisCon.Close()
		log.Debugln("Blocking key:", key)
		setnx_check, err := redis.Int(masterRedisCon.Do("setnx", key, ""))
		if err != nil {
			log.Warnln("Failed to perform operation on Master Redis, error: ", err)
			is.once.Do(is.closeClient)
			return err
		}
		if setnx_check == 1 {
			_, err := masterRedisCon.Do("expire", key, is.blocktimeout)
			if err != nil {
				log.Warnln("Failed to expire the key, error: ", err)
				is.once.Do(is.closeClient)
				return err
			}
		}
	}
	return nil
}
