package main

import (
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"github.com/pquerna/ffjson/ffjson"
	"strings"
)

func (is ClientInfo) sendDataUpdates(cid string) {
	log := is.log.WithFields(logrus.Fields{"func": "sendDataUpdates"})
	//data, err := json.Marshal(whiteListURLsmap[cid])
	data, err := ffjson.Marshal(whiteListURLsmap[cid])
	if err != nil {
		log.Warnln("failed to unmarshal whitelist URLs", err)
	} else {
		select {
		case is.wsWriteCh <- append([]byte("WHITELISTURLS:"), data...):
		case <-is.doneCh:
			return
		}
	}

	data, err = ffjson.Marshal(whiteListIPmap[cid])
	if err != nil {
		log.Warnln("failed to unmarshal whitelist IPs", err)
	} else {
		select {
		case is.wsWriteCh <- append([]byte("WHITELISTIPS:"), data...):
		case <-is.doneCh:
			return
		}

	}

	data, err = ffjson.Marshal(blacklistIPmap[cid])
	if err != nil {
		log.Warnln("failed to unmarshal Blacklist IPs", err)
	} else {
		select {
		case is.wsWriteCh <- append([]byte("BLACKLISTIPS:"), data...):
		case <-is.doneCh:
			return
		}
	}
}

func (is ClientInfo) redispubsub() {
	defer is.defaultWG.Done()
	log := is.log.WithFields(logrus.Fields{"func": "redispubsub"})
	defer log.Debugln("terminated goroutine")
	redisMessageCh := make(chan string)
	con := localRedisPool.Get()
	psc := redis.PubSubConn{con}
	psc.PSubscribe("__keyspace@0__:blocked:cid:" + is.cid + ":*")
	//psc.PSubscribe("__keyspace@0__:useragent:cid:" + is.cid + ":*")
	go func() {
		defer log.Debugln("exited redis pubsub")
		defer psc.PUnsubscribe()
		for con.Err() == nil {
			switch v := psc.Receive().(type) {
			//case redis.Message:
			//	messageCh <- string(v.Data)
			case redis.PMessage:
				messageType := string(v.Data)
				//log.Printf(messageType)
				message := string(v.Channel)
				//log.Println(message)
				dataToSend, err := is.cleanPMessageData(message, messageType, is.cid)
				if err != nil {
					log.Warnln("Failed to parse the message: %s err: %s", message, err)
				} else if dataToSend == "" {
				} else {
					select {
					case redisMessageCh <- dataToSend:
					case <-is.doneCh:
						return
					}

				}
			case redis.Subscription:
				log.Debugln("subscription: %s %s %d \n", v.Kind, v.Channel, v.Count)
				if v.Count == 0 {
					return
				}
			case error:
				log.Warnln("err occured while reading the message from redis")
				return
			}
		}
	}()
	for {
		select {
		case message := <-redisMessageCh:
			select {
			case is.wsWriteCh <- []byte(message):
			case <-is.doneCh:
				err := psc.PUnsubscribe()
				if err != nil {
					log.Debugln("Error occured while closing pubsub: ", err)
				}
				return
			}
		case <-is.doneCh:
			err := psc.PUnsubscribe()
			if err != nil {
				log.Debugln("error occured while closing pubsub:", err)
			}
			return
		}
	}
}

func (is *ClientInfo) cleanPMessageData(message string, messageType string, cid string) (string, error) {
	log := is.log.WithFields(logrus.Fields{"func": "cleanPMessage"})
	defer log.Debugln("Exit func")
	if strings.Contains(message, "blocked") {
		message = strings.TrimPrefix(message, "__keyspace@0__:")
		message = strings.TrimPrefix(message, "blocked:cid:"+cid+":")
		data, err := is.setipCount(message)
		if err != nil {
			return "", err
		}
		if messageType == "set" {
			return "IPCOUNTBLOCK:" + data, err
		} else if messageType == "del" {
			return "IPCOUNTUNBLOCK:" + data, err
		} else if messageType == "expire" {
			return "", nil
		} else {
			return "", fmt.Errorf("Message Type not found!")
		}
	}
	//	else if strings.Contains(message, "useragent:") {
	//	message = strings.TrimPrefix(message, "__keyspace@0__:")
	//	message = strings.TrimPrefix(message, "useragent:cid:"+cid+":")
	//	data, err := uaMapBlock(message)
	//	if err != nil {
	//		return "", err
	//	}
	//	if messageType == "set" {
	//		return "UAUPDATE:" + data, err
	//	}
	//}
	return "", nil
}

func (is *ClientInfo) setipCount(message string) (string, error) {
	log := is.log.WithFields(logrus.Fields{"func": "ipCountBlock"})
	defer log.Debugln("Exit func")
	splitdata := strings.Split(message, ":")
	if len(splitdata) == 2 {
		blockedData := new(runTimeBlockedIP)
		blockedData.Ip = splitdata[0]
		blockedData.Ua = splitdata[1]
		blockedData.Reason = "realTime"
		blockedData.Action = "block"
		data, err := ffjson.Marshal(blockedData)
		if err != nil {
			log.Warnf("failed to marshal the blockedData, %#v", blockedData)
			return "", err
		}
		return string(data), err
	} else {
		return "", errors.New("message split not equal to 2")
	}
}

//func uaMapBlock(message string) (string, error) {
//	//log.Println(message)
//	splitdata := strings.Split(message, ":")
//	if len(splitdata) == 2 {
//		uamap := new(UAdata)
//		uamap.Ua = splitdata[0]
//		uamap.UaParsed = splitdata[1]
//		data, err := ffjson.Marshal(uamap)
//		if err != nil {
//			log.Warnf("failed to marshal uamap %#v", uamap)
//		}
//		return string(data), err
//	} else {
//		return "", errors.New("message split not equal to 2")
//	}
//}

func (is *ClientInfo) sendRedisUpdates() {
	log := is.log.WithFields(logrus.Fields{"func": "sendRedisUpdates"})
	log.Debugln("Exit func")
	blockedIPs, err := getAllMatchingKeys("blocked:cid:" + is.cid + ":*")
	if err != nil {
		log.Warnln("failed to perform keys operation")
		return
	}
	for _, blockedIP := range blockedIPs {
		blockedIP = strings.TrimPrefix(blockedIP, "blocked:cid:"+is.cid+":")
		messageToSend, err := is.setipCount(blockedIP)
		if err != nil {
			log.Warnln("failed to convert the information to json format")
		} else {
			select {
			case is.wsWriteCh <- []byte("IPCOUNTBLOCK:" + messageToSend):
			case <-is.doneCh:
				return
			}
		}
	}

	//useragents, err := getAllMatchingKeys("useragent:cid:" + is.cid + ":*")
	//if err != nil {
	//	log.Warnln("failed to perform keys operation")
	//	return
	//}
	//for _, useragent := range useragents {
	//	useragent = strings.TrimPrefix(useragent, "useragent:cid:"+is.cid+":")
	//	messageToSend, err := uaMapBlock(useragent)
	//	if err != nil {
	//		log.Warnln("failed to convert the information to json format")
	//	} else {
	//		select {
	//		case is.wsWriteCh <- []byte("UAUPDATE:" + messageToSend):
	//		case <-is.doneCh:
	//			return
	//		}
	//	}
	//}
}
