package main

import (
	"github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func getMongoConfig() error {
	log := log.WithFields(logrus.Fields{"func": "getMongoConfig"})
	log.Debugln("Exit func")
	var wlurl []whiteListedURLs

	session := MongoSession.Copy()
	defer session.Close()

	c := session.DB("config").C("wlurls")
	c.Find(bson.M{}).All(&wlurl)
	for _, data := range wlurl {
		whiteListURLsmap[data.Cid] = data
	}
	var blips []blackListedIPs
	c = session.DB("config").C("blips")
	c.Find(bson.M{}).All(&blips)
	for _, data := range blips {
		blacklistIPmap[data.Cid] = data
	}

	var wlips []whiteListedIPs
	c = session.DB("config").C("wlips")
	c.Find(bson.M{}).All(&wlips)
	for _, data := range wlips {
		whiteListIPmap[data.Cid] = data
	}
	//j, _:= json.Marshal(whiteListURLsmap["123456"])
	log.Debugf("whiteListURLsmap: %#v", whiteListURLsmap)
	//
	//j, _ = json.Marshal(whiteListIPmap["123456"])
	log.Debugf("whiteListIPmap: %#v", whiteListIPmap)
	//
	//j, _ = json.Marshal(blacklistIPmap["123456"])
	log.Debugf("blacklistIPmap: %#v", blacklistIPmap)
	return nil
}

func (is *ClientInfo) mongodbWorker() {
	log := is.log.WithFields(logrus.Fields{"func": "mongodbWorker"})
	defer is.defaultWG.Done()
	defer log.Debugln("Exit Worker")
	for {
		select {
		case requestdata := <-is.mongodbCh:
			// Add the data to mongodb
			if requestdata.BlockType == "" {
				requestdata.checkWG.Wait()
			}
			func() {
				session := MongoSession.Copy()
				defer session.Close()

				collection := "CID" + is.cid + "_" + time.Now().Format("02Jan2006")
				c := session.DB("trans").C(collection)
				err := c.Insert(requestdata)

				if err != nil {
					log.Warnln("unable to write data to mongodb: %v err: %s \n", requestdata, err)
					// TODO: may need to disconnect the agent
				}
			}()
		case <-is.doneCh:
			return
		}
	}
}
