// Package udger package allow you to load in memory and lookup the user agent database to extract value from the provided user agent
package main

import (
	"errors"
	"strings"
	"github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
	"github.com/garyburd/redigo/redis"
	"strconv"
	"fmt"
)

// New creates a new instance of Udger and load all the database in memory to allow fast lookup
// you need to pass the sqlite database in parameter
func NewUAParser(redisIp string, redisPort int) (*Udger, error) {
	u := &Udger{
		Browsers:     make(map[int]Browser),
		OS:           make(map[int]OS),
		Devices:      make(map[int]Device),
		browserTypes: make(map[int]string),
		browserOS:    make(map[int]int),
	}
	var err error

	//if _, err := os.Stat(dbPath); os.IsNotExist(err) {
	//	return nil, err
	//}

	//u.db, err = sql.Open("sqlite3", dbPath)
	//if err != nil {
	//	return nil, err
	//}
	//defer u.db.Close()

	redisCon, err := redis.Dial("tcp", redisIp+":"+strconv.Itoa(redisPort))
	if err != nil {
		panic("unable to connect")
	}
	err = u.init(redisCon)
	if err != nil {
		return nil, err
	}

	return u, nil
}

// Lookup one user agent and return a Info struct who contains all the metadata possible for the UA.
func (udger *Udger) Lookup(ua string) (*Info, error) {
	info := &Info{}

	browserID, version, err := udger.findDataWithVersion(ua, udger.rexBrowsers, true)
	if err != nil {
		return nil, err
	}

	info.Browser = udger.Browsers[browserID]
	info.Browser.Name = info.Browser.Family + " " + version
	info.Browser.Version = version
	info.Browser.Type = udger.browserTypes[info.Browser.typ]

	if val, ok := udger.browserOS[browserID]; ok {
		info.OS = udger.OS[val]
	} else {
		osID, _, err := udger.findData(ua, udger.rexOS, false)
		if err != nil {
			return nil, err
		}
		info.OS = udger.OS[osID]
	}

	deviceID, _, err := udger.findData(ua, udger.rexDevices, false)
	if err != nil {
		return nil, err
	}
	if val, ok := udger.Devices[deviceID]; ok {
		info.Device = val
	} else if info.Browser.typ == 3 { // if browser is mobile, we can guess its a mobile
		info.Device = Device{
			Name: "Smartphone",
			Icon: "phone.png",
		}
	} else if info.Browser.typ == 5 || info.Browser.typ == 10 || info.Browser.typ == 20 || info.Browser.typ == 50 {
		info.Device = Device{
			Name: "Other",
			Icon: "other.png",
		}
	} else {
		//nothing so personal computer
		info.Device = Device{
			Name: "Personal computer",
			Icon: "desktop.png",
		}
	}

	return info, nil
}

func (udger *Udger) cleanRegex(r string) string {
	if strings.HasSuffix(r, "/si") {
		r = r[:len(r)-3]
	}
	if strings.HasPrefix(r, "/") {
		r = r[1:]
	}

	return r
}

func (udger *Udger) findDataWithVersion(ua string, data []rexData, withVersion bool) (idx int, value string, err error) {
	defer func() {
		if r := recover(); r != nil {
			idx, value, err = udger.findData(ua, data, false)
		}
	}()

	idx, value, err = udger.findData(ua, data, withVersion)

	return idx, value, err
}

func (udger *Udger) findData(ua string, data []rexData, withVersion bool) (idx int, value string, err error) {
	for i := 0; i < len(data); i++ {
		data[i].Regex = udger.cleanRegex(data[i].Regex)
		r, err := pcre.Compile(data[i].Regex, pcre.CASELESS)
		if err != nil {
			return -1, "", errors.New(err.String())
		}
		matcher := r.MatcherString(ua, 0)
		if !matcher.MatchString(ua, 0) {
			continue
		}

		if withVersion && matcher.Present(1) {
			return data[i].ID, matcher.GroupString(1), nil
		}

		return data[i].ID, "", nil
	}

	return -1, "", nil
}

func (udger *Udger) init(redisCon redis.Conn) error {
	//rows, err := udger.db.Query("SELECT client_id, regstring FROM udger_client_regex ORDER by sequence ASC")
	values, err := redis.Strings(redisCon.Do("smembers", "ua_client_regex"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d rexData
		result := strings.Split(value, "||")
		d.ID, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.Regex = result[1]
		udger.rexBrowsers = append(udger.rexBrowsers, d)
	}

	values, err = redis.Strings(redisCon.Do("smembers", "ua_deviceclass_regex"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d rexData
		result := strings.Split(value, "||")
		d.ID, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.Regex = result[1]
		udger.rexDevices = append(udger.rexDevices, d)
	}

	values, err = redis.Strings(redisCon.Do("smembers", "ua_os_regex"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d rexData
		result := strings.Split(value, "||")
		d.ID, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.Regex = result[1]
		udger.rexOS = append(udger.rexOS, d)
	}

	values, err = redis.Strings(redisCon.Do("smembers", "ua_client_list"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d Browser
		var id int
		result := strings.Split(value, "||")
		id, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.typ, _ = strconv.Atoi(result[1])
		d.Family = result[2]
		d.Engine = result[3]
		d.Company = result[4]
		d.Icon = result[5]
		udger.Browsers[id] = d
	}

	values, err = redis.Strings(redisCon.Do("smembers", "ua_os_list"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d OS
		var id int
		result := strings.Split(value, "||")
		id, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.Name = result[1]
		d.Family = result[2]
		d.Company = result[3]
		d.Icon = result[4]
		udger.OS[id] = d
	}


	values, err = redis.Strings(redisCon.Do("smembers", "ua_deviceclass_list"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d Device
		var id int
		result := strings.Split(value, "||")
		id, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d.Name = result[1]
		d.Icon = result[2]
		udger.Devices[id] = d
	}


	values, err = redis.Strings(redisCon.Do("smembers", "ua_client_class"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var d string
		var id int
		result := strings.Split(value, "||")
		id, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		d = result[1]
		udger.browserTypes[id] = d
	}

	values, err = redis.Strings(redisCon.Do("smembers", "ua_client_os_relation"))
	if err != nil {
		return err
	}
	for _, value := range values{
		var browser int
		var os int
		result := strings.Split(value, "||")
		browser, err = strconv.Atoi(result[0])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		os, err = strconv.Atoi(result[1])
		if err != nil {
			fmt.Println("failed to convert string to integer")
		}
		udger.browserOS[browser] = os
	}
	return nil
}
