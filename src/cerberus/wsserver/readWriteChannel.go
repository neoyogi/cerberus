package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/websocket"
	"github.com/pquerna/ffjson/ffjson"
	"mimeparse"
	"strings"
	"sync"
)

func (is *ClientInfo) readMessages() {
	log := is.log.WithFields(logrus.Fields{"func": "readMessage"})
	defer is.defaultWG.Done()
	defer log.Debugln("Exit worker")
	readmessageWG := new(sync.WaitGroup)
	readmessageWG.Add(1)
	msgCh := make(chan []byte)
	go func() {
		defer readmessageWG.Done()
		defer log.Debugln("readMessages sub goroutine ended")
		for {
			_, message, err := is.con.ReadMessage()
			if err != nil {
				log.Warnln("failed to read input message: ", err)
				is.once.Do(is.closeClient)
				return
			}
			log.Debugln("message :", string(message))
			select {
			case msgCh <- message:
			case <-is.doneCh:
				return
			}

		}
	}()
	for {
		select {
		case <-is.doneCh:
			is.con.Close()
			return
		case msg := <-msgCh:
			// Check whether redisCh and/or mongodbCh are full
			if len(is.redisCh) == cap(is.redisCh) || len(is.mongodbCh) == cap(is.mongodbCh) {
				log.Warnln("Redis/Mongodb channles are full: redisCh=%d, mongodbCh=%d", len(is.redisCh), len(is.mongodbCh))
				is.once.Do(is.closeClient)
				return

			}
			requestdata := new(RequestData)
			err := ffjson.Unmarshal(msg, requestdata)
			if err != nil {
				log.Warnln("failed to unmarshal the data: %s err: %s \n", string(msg), err)
				continue
			}
			if requestdata.Accept != "" {
				acceptFields := mimeparse.ParseList(requestdata.Accept)
				acceptList := mimeparse.ParseAccept(acceptFields)
				if len(acceptList) > 0 {
					for _, acceptSpec := range acceptList {
						if strings.Contains(acceptSpec.Value, "html") {
							requestdata.AcceptTypeHTML = true
						}
					}
				}
			}
			if requestdata.BlockType == "" {
				requestdata.checkDBWG = new(sync.WaitGroup)
				requestdata.checkDBWG.Add(1)
				select {
				case <-is.doneCh:
					requestdata.checkDBWG.Done()
					is.con.Close()
					return
				case is.redisCh <- requestdata:
				}

				requestdata.checkDBWG.Add(1)
				select {
				case <-is.doneCh:
					requestdata.checkDBWG.Done()
					is.con.Close()
					return
				case is.ipCheckCh <- requestdata:
				}

				requestdata.checkDBWG.Add(1)
				select {
				case <-is.doneCh:
					requestdata.checkDBWG.Done()
					is.con.Close()
					return
				case is.uaParseCh <- requestdata:
				}

				requestdata.checkWG = new(sync.WaitGroup)
				requestdata.checkWG.Add(1)
				select {
				case <-is.doneCh:
					requestdata.checkWG.Done()
					is.con.Close()
					return
				case is.validationCh <- requestdata:
				}
			}

			select {
			case <-is.doneCh:
				is.con.Close()
				return
			case is.mongodbCh <- requestdata:
			}
		}
	}
	readmessageWG.Wait()
}

func (is *ClientInfo) writeMessages() {
	log := is.log.WithFields(logrus.Fields{"func": "writeMessages"})
	defer is.defaultWG.Done()
	defer log.Debugln("Exit worker")
	for {
		select {
		case msg := <-is.wsWriteCh:
			log.Debugln("message to write:", string(msg))
			err := is.con.WriteMessage(websocket.TextMessage, []byte(msg))
			if err != nil {
				log.Warnln("failed to write message, error: ", err)
				is.once.Do(is.closeClient)
				return
			}
		case <-is.doneCh:
			return
		}
	}
}
