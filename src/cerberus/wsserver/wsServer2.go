package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"github.com/rifflock/lfshook"
	"gopkg.in/mgo.v2"
	"net/http"
	"strconv"
	"sync"
	"time"
)

var log = logrus.New()

func init() {
	flag.Parse()

	log.Formatter = new(logrus.TextFormatter) // default
	log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{logrus.DebugLevel: "cerberus.log", logrus.InfoLevel: "cerberus.log", logrus.WarnLevel: "cerberus.log", logrus.ErrorLevel: "cerberus.log"}))
	log.Level = getLogLevel(*logLevel)
}

func getLogLevel(level string) logrus.Level {
	switch loglevel := level; loglevel {
	case "debug":
		return logrus.DebugLevel
	case "info":
		return logrus.InfoLevel
	case "warning":
		return logrus.WarnLevel
	}
	return logrus.DebugLevel
}

func startWSserver(uri string, host string, port int, wg *sync.WaitGroup) {
	log := log.WithFields(logrus.Fields{"func": "startWSserver"})
	defer wg.Done()
	log.WithFields(logrus.Fields{"uri": uri, "host": host, "port": port}).Debugln("spawing goroutine to handle websocket")
	http.HandleFunc(uri, wsrequestProc)
	http.ListenAndServe(host+":"+strconv.Itoa(port), nil)
}

func getMongodbSession() error {
	mongodbHost := hgetRediskeyString("mongodb", "ip")
	mongodbPort := hgetRediskeyString("mongodb", "port")

	// We need this object to establish a session to our MongoDB.
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:   []string{mongodbHost + ":" + mongodbPort},
		Timeout: 60 * time.Second,
		//Database: AuthDatabase,
		//Username: AuthUserName,
		//Password: AuthPassword,
		//PoolLimit: 100,
	}

	log.WithFields(logrus.Fields{"mongodbHost": mongodbHost, "mongodbport": mongodbPort}).Debugln("connecting to Mongodb")
	// Create a session which maintains a pool of socket connections to our MongoDB.
	var err error
	MongoSession, err = mgo.DialWithInfo(mongoDBDialInfo)

	if err != nil {
		log.WithFields(logrus.Fields{"mongodbHost": mongodbHost, "mongodbport": mongodbPort}).Warningln("Failed to connect to Mongodb server")
		return err
	}
	MongoSession.SetMode(mgo.Eventual, true)
	log.Debugln("MongoDB connection is successful")
	return nil
}

func main() {
	defer log.Warnln("Websocket Server is down!")
	log := log.WithFields(logrus.Fields{"func": "main"})
	localRedisPool = newPool("127.0.0.1", 6379, 120, 0)
	err := configRedisForExpireEvents()
	if err != nil {
		log.Errorf("Failed to configure redis for expire events %s", err)
	}

	// get mongodb session and config
	getMongodbSession()
	getMongoConfig()

	mainWG := new(sync.WaitGroup)
	mainWG.Add(1)
	log.Debug("starting WebSocket Server")
	go startWSserver("/ws", "0.0.0.0", 3000, mainWG)
	mainWG.Add(1)
	go mgrServer(mainWG)
	mainWG.Wait()
}
