package main

import (
	"github.com/Sirupsen/logrus"
	//"github.com/pquerna/ffjson/ffjson"
	//"io/ioutil"
	//"net"
	//"net/http"
	//"net/url"
	//"time"
)

func (is *ClientInfo) uaParserWorker() {
	defer is.defaultWG.Done()
	log := is.log.WithFields(logrus.Fields{"func": "uaParserWorker"})
	defer log.Debugln("Exit worker")

	for {
		select {
		case requestdata := <-is.uaParseCh:
			if requestdata.BlockType != "" {
				continue
			}
			func() {
				defer requestdata.checkDBWG.Done()
				is.parseUA(requestdata)
			}()

		case <-is.doneCh:
			return
		}
	}
}

func (is *ClientInfo) parseUA(requestdata *RequestData) {
	log := is.log.WithFields(logrus.Fields{"func": "parseUA"})
	defer log.Debugln("Exit")
	//Url, err := url.Parse("http://" + is.uaServerIp + ":" + is.uaServerPort)
	//if err != nil {
	//	log.Debugln("failed to parse url")
	//}
	//Url.Path += "/ua"
	//parameters := url.Values{}
	//parameters.Add("uas", requestdata.Ua)
	//Url.RawQuery = parameters.Encode()
	//netTransport := &http.Transport{Dial: (&net.Dialer{Timeout: time.Millisecond * 500}).Dial}
	//netClient := &http.Client{Timeout: time.Second, Transport: netTransport}
	//response, err := netClient.Get(Url.String())
	//if err != nil {
	//	log.Warnln("unable to query User Agent Parser server, error: ", err)
	//	return
	//}
	//defer response.Body.Close()
	//body, err := ioutil.ReadAll(response.Body)
	//if err != nil {
	//	log.Warnln("failed to read the body from the response, err:", err)
	//	return
	//}
	//uadata := new(UaData)
	//err = ffjson.Unmarshal(body, uadata)
	//if err != nil {
	//	log.Warnf("failed to unmarshall User agent parsing data from UAserver, err: %s data: %#v", err, body)
	//}
	//requestdata.UaParsed = uadata
	parser, err := NewUAParser("127.0.0.1", 6379)
	if err != nil{
		log.Warnln("failed start a new parser, error: ", err)
	}
	result, err := parser.Lookup(requestdata.Ua)
	if err != nil{
		log.Warnln("failed to parse UA, error: ", err)
	}
	log.Warnln(result)
}
