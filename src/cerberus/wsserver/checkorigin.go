package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"net/http"
)

func checkOrigin(r *http.Request) bool {
	log := log.WithFields(logrus.Fields{"func": "checkOrigin"})
	defer log.Debugln("Exit func")
	originCID := r.Header.Get("Origin")
	log = log.WithFields(logrus.Fields{"cid": originCID})
	if len(originCID) == 0 {
		log.Warnln("No CID provided by the client")
		return false
	}
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	log.Debug("checking if the CID exists in redis")
	result, err := redis.Bool(redisCon.Do("exists", "cid:"+originCID))
	if err != nil {
		log.Warnln("failed to perform exists query on cid: %s using the redis con: %#v", originCID, redisCon)
		return false
	}
	log.Infoln("client CID validated: result: ", result)
	return result
}
