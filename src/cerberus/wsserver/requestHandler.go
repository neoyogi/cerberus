package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/websocket"
	"github.com/rifflock/lfshook"
	"net/http"
	"strconv"
	"sync"
)

func wsrequestProc(w http.ResponseWriter, r *http.Request) {
	log := log.WithFields(logrus.Fields{"func": "wsrequestProc"})
	log.Infof("======   New Connection   ==========")
	defer log.Infof("=======   Connection Ends  ==========")
	var upgrader = websocket.Upgrader{
		CheckOrigin:     func(r *http.Request) bool { return checkOrigin(r) },
		ReadBufferSize:  readBufferSize,
		WriteBufferSize: writeBufferSize,
	}
	cid := r.Header.Get("Origin")
	sid := r.Header.Get("_bhsid")
	log = log.WithFields(logrus.Fields{"cid": cid, "sid": sid})
	con, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Warnln("error occured while upgrading to websocket %s", err)
		return
	}
	clientInfo := new(ClientInfo)

	clientInfo.closeClient = func() {
		log := clientInfo.log.WithFields(logrus.Fields{"func": "closeClient"})
		log.Warnln("closing client connection")
		if clientInfo.isdoneChClosed == false {
			clientInfo.log.Warnln("closing the done channel")
			clientInfo.isdoneChClosed = true
			close(clientInfo.doneCh)
		}
	}

	clientInfo.cid = cid
	clientInfo.sid = sid
	clientInfo.logger = logrus.New()
	clientInfo.logger.Formatter = new(logrus.TextFormatter)
	clientInfo.logger.Hooks.Add(lfshook.NewHook(lfshook.PathMap{logrus.DebugLevel: "cerberus_" + cid + ".log", logrus.InfoLevel: "cerberus_" + cid + ".log", logrus.WarnLevel: "cerberus_" + cid + ".log", logrus.ErrorLevel: "cerberus_" + cid + ".log"}))
	clientInfo.logger.Level = getLogLevel(*logLevel)
	clientInfo.log = logrus.NewEntry(clientInfo.logger).WithFields(logrus.Fields{"cid": clientInfo.cid, "sid": clientInfo.sid})
	clientInfo.con = con
	clientInfo.wsWriteCh = make(chan []byte)
	clientInfo.redisCh = make(chan *RequestData, 1000)
	clientInfo.mongodbCh = make(chan *RequestData, 1000)
	clientInfo.ipCheckCh = make(chan *RequestData, 1000)
	clientInfo.validationCh = make(chan *RequestData, 1000)
	clientInfo.uaParseCh = make(chan *RequestData, 1000)
	clientInfo.defaultWG = new(sync.WaitGroup)
	clientInfo.redisWorkers = hgetRediskeyInt("cid:"+cid, "redisworkers")
	clientInfo.mongodbWorkers = hgetRediskeyInt("cid:"+cid, "mongodbworkers")
	clientInfo.decisionWorkers = hgetRediskeyInt("cid:"+cid, "decisionworkers")
	clientInfo.uaParserWorkers = hgetRediskeyInt("cid:"+cid, "uaparserworkers")
	clientInfo.ipValidationWorkers = hgetRediskeyInt("cid:"+cid, "ipvalidationworkers")
	clientInfo.doneCh = make(chan struct{})
	clientInfo.thresholdCount1 = hgetRediskeyInt("cid:"+cid, "thresholdcount1")
	clientInfo.thresholdCount2 = hgetRediskeyInt("cid:"+cid, "thresholdcount2")
	clientInfo.thresholdTime1 = hgetRediskeyInt("cid:"+cid, "thresholdtime1")
	clientInfo.thresholdTime2 = hgetRediskeyInt("cid:"+cid, "thresholdtime2")
	clientInfo.masterRedisIP = getMasterRedisIP()
	clientInfo.masterRedisPort = getMasterRedisPort()
	clientInfo.jsServerIp = hgetRediskeyString("cid:"+cid, "jsserver_ip")
	clientInfo.jsServerPort = hgetRediskeyString("cid:"+cid, "jsserver_port")
	clientInfo.uaServerIp = hgetRediskeyString("cid:"+cid, "uaserverip")
	clientInfo.uaServerPort = hgetRediskeyString("cid:"+cid, "uaserverport")
	clientInfo.blocktimeout = hgetRediskeyInt("cid:"+cid, "blocktimeout")

	clients[cid+":"+sid] = clientInfo
	clientInfo.log.Debugln("new client information added to clients map", clients)

	clientInfo.log.Debugln("complete clientInfo struct information: ", clientInfo)

	if pool, ok := detectionRedisPool[cid]; !ok {
		clientInfo.log.Debugln("DetectionRedisPool map doesn't contain the pool, creating new pool", detectionRedisPool)
		detectionRedisPool[cid] = newPool(hgetRediskeyString("cid:"+cid, "destredis_ip"), hgetRediskeyInt("cid:"+cid, "destredis_port"), 120, 200)
		clientInfo.pool = detectionRedisPool[cid]
	} else {
		clientInfo.log.Debugln("using the existing detection redis pool", detectionRedisPool)
		clientInfo.pool = pool
	}

	if pool, ok := masterRedisPool[cid]; !ok {
		clientInfo.log.Debugln("masterRedisPool doesn't contain pool for CID, creating new", masterRedisPool)
		masterRedisPort, _ := strconv.Atoi(clientInfo.masterRedisPort)
		masterRedisPool[cid] = newPool(clientInfo.masterRedisIP, masterRedisPort, 120, 200)
		clientInfo.masterRedisPool = masterRedisPool[cid]
	} else {
		clientInfo.log.Debugln("using existing MasterRedisPool ", masterRedisPool)
		clientInfo.masterRedisPool = pool
	}

	clientInfo.log.Infof("starting %d redisWorkers: ", clientInfo.redisWorkers)
	for i := 1; i <= clientInfo.redisWorkers; i++ {
		clientInfo.defaultWG.Add(1)
		go clientInfo.redisWorker()
	}

	clientInfo.log.Infof("starting %d uaParserWorker ", clientInfo.uaParserWorkers)
	for i := 1; i <= clientInfo.uaParserWorkers; i++ {
		clientInfo.defaultWG.Add(1)
		go clientInfo.uaParserWorker()
	}

	clientInfo.log.Infof("starting %d mongodbWorkers: ", clientInfo.mongodbWorkers)
	for i := 1; i <= clientInfo.mongodbWorkers; i++ {
		clientInfo.defaultWG.Add(1)
		go clientInfo.mongodbWorker()
	}

	clientInfo.log.Infof("starting %d ipValidationworkers: ", clientInfo.ipValidationWorkers)
	for i := 1; i <= clientInfo.ipValidationWorkers; i++ {
		clientInfo.defaultWG.Add(1)
		go clientInfo.ipValidationWorker()
	}

	clientInfo.log.Infoln("Starting %d decision workers", clientInfo.decisionWorkers)
	for i := 1; i <= clientInfo.decisionWorkers; i++ {
		clientInfo.defaultWG.Add(1)
		go clientInfo.decisionWorker()
	}

	clientInfo.log.Infoln("starting readMessages goroutine")
	clientInfo.defaultWG.Add(1)
	go clientInfo.readMessages()

	clientInfo.log.Infoln("starting writeMessage goroutine")
	clientInfo.defaultWG.Add(1)
	go clientInfo.writeMessages()

	clientInfo.log.Infoln("SendDataUpdates")
	clientInfo.sendDataUpdates(cid)
	clientInfo.sendRedisUpdates()

	clientInfo.defaultWG.Add(1)
	//go clientInfo.processRedisEvents()
	go clientInfo.redispubsub()

	clientInfo.defaultWG.Wait()

	clientInfo.log.Debugln("deleting client from clients map", clients)
	delete(clients, cid+":"+sid)
	clientInfo.log.Debugln("after deleting client from clients map", clients)
	clientInfo.log.Infoln("connetions ends")
}
