package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"strconv"
	"strings"
)

func newPool(host string, port int, timeout int, maxactive int) *redis.Pool {
	log := log.WithFields(logrus.Fields{"func": "newPool", "host": host, "port": port, "timeout": timeout, "maxactive": maxactive})
	log.Debugln("Creating new pool")
	return &redis.Pool{
		MaxIdle:   timeout,
		MaxActive: maxactive, // max number of connections
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", host+":"+strconv.Itoa(port))
			if err != nil {
				log.Warnln("Failed to dial: ", err.Error())
			}
			return c, err
		},
	}
}

func getRedisKeystring(key string) string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.String(redisCon.Do("get", key))
	if err != nil {
		log.Warnln("Failed to perform get operation on redis", err)
	}
	return result
}

func getRedisKeyInt(key string) int {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.Int(redisCon.Do("get", key))
	if err != nil {
		log.Warnln("Failed to perform get operation on redis", err)
	}
	return result
}

func hgetRediskeyInt(key string, field string) int {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.Int(redisCon.Do("hget", key, field))
	if err != nil {
		log.Warnln("failed to perform hget operation on redis", err)
	}
	return result
}

func hgetRediskeyString(key string, field string) string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.String(redisCon.Do("hget", key, field))
	if err != nil {
		log.Warnln("failed to perform hget operation on redis", err)
	}
	return result
}

func hmgetRediskeyString(key string) []string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.Strings(redisCon.Do("hgetall", key))
	if err != nil {
		log.Warnln("failed to perform hmget, err: ", err)
	}
	return result
}

func getAllMatchingKeys(key string) ([]string, error) {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redisCon.Do("keys", key)
	data, err := redis.Strings(result, err)
	if err != nil {
		log.Warnln("Failed to get the result for the keys query, err :  %s \n", err)
	}
	return data, err
}

func getMasterRedisIP() string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	returnData, err := redis.String(redisCon.Do("info"))
	if err != nil {
		log.Warnln("failed to execute the command info on local redis server", err)
		return ""
	}
	matchResults := master_ip.FindStringSubmatch(returnData)
	if len(matchResults) < 4 {
		log.Warnln("the parse doesn't include Master redis ip")
		return ""
	}
	return strings.Join(matchResults[1:], ".")
}

func getMasterRedisPort() string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	returnData, err := redis.String(redisCon.Do("info"))
	if err != nil {
		log.Warnln("failed to execute the command info on local redis server", err)
		return ""
	}
	matchResults := master_port.FindStringSubmatch(returnData)
	if len(matchResults) < 2 {
		log.Warnln("Couldn't get the master port number")
		return ""
	}
	return matchResults[len(matchResults)-1]
}

func configRedisForExpireEvents() error {
	log := log.WithFields(logrus.Fields{"func": "configRedisForExpireEvents"})
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	_, err := redisCon.Do("CONFIG", "set", "notify-keyspace-events", "KA")
	if err != nil {
		log.Warnln("failed to set notify-keyspace-events setting")
	}
	return err
}

func getZrangeByScore(key string, lower_range interface{}, upper_range interface{}, lower_limit int, upper_limit int) ([]string, error) {
	log := log.WithFields(logrus.Fields{"func": "getzrange"})
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redisCon.Do("zrangebyscore", key, lower_range, upper_range, "limit", lower_limit, upper_limit)
	data, err := redis.Strings(result, err)
	if err != nil {
		log.Warnln("failed to perform getZrange query on local redis server")
		return []string{}, err
	}
	return data, nil
}
