package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/websocket"
	"gopkg.in/mgo.v2"
	"net/http"
	"regexp"
	"sync"
)

type ClientInfo struct {
	cid string
	con *websocket.Conn
	//wsReadCh chan []byte
	wsWriteCh chan []byte
	//redisCh chan []byte
	//mongodbCh chan []byte
	redisCh             chan *RequestData
	mongodbCh           chan *RequestData
	ipCheckCh           chan *RequestData
	validationCh        chan *RequestData
	uaParseCh           chan *RequestData
	defaultWG           *sync.WaitGroup
	redisWriteCh        chan string
	redisWorkers        int
	mongodbWorkers      int
	decisionWorkers     int
	uaParserWorkers     int
	ipValidationWorkers int
	doneCh              chan struct{}
	isdoneChClosed      bool
	sid                 string
	pool                *redis.Pool
	masterRedisPool     *redis.Pool
	thresholdCount1     int
	thresholdTime1      int
	thresholdCount2     int
	thresholdTime2      int
	masterRedisIP       string
	masterRedisPort     string
	jsServerIp          string
	jsServerPort        string
	uaServerIp          string
	uaServerPort        string
	blocktimeout        int
	loglevel            string
	once                sync.Once
	closeClient         func()
	log                 *logrus.Entry
	logger              *logrus.Logger
}

var (
	clients         = make(map[string]*ClientInfo)
	localRedisPool  *redis.Pool
	readBufferSize  int = 2048
	writeBufferSize int = 2048
)

var (
	detectionRedisPool = make(map[string]*redis.Pool)
	masterRedisPool    = make(map[string]*redis.Pool)
)

//type UAdata struct {
//	Ua       string `json:"ua"`
//	UaParsed string `json:"uaparsed"`
//}

type RequestData struct {
	checkDBWG         *sync.WaitGroup
	checkWG           *sync.WaitGroup
	Ip                string `json:"ip"`
	Ua                string `json:"ua" bson:"ua,omitempty"`
	Url               string `json:"url" bson:"url,omitempty"`
	UaParsed          *UaData
	IpParsed          *IPdata
	Uid               string   `json:"uid"`
	Referrer          string   `json:"referrer" bson:"referrer,omitempty"`
	Method            string   `json:"method"`
	ContentType       string   `json:"content_type" bson:"content_type,omitempty"`
	Forwarded         string   `json:"forwarded" bson:"forwarded,omitempty"`
	Origin            string   `json:"origin" bson:"origin,omitempty"`
	Host              string   `json:"host" bson:"host,omitempty"`
	Accept            string   `json:"accept" bson:"accept,omitempty"`
	AcceptTypeHTML    bool     `json:"accept_type_html" bson:"accept_type_html,omitempty"`
	ForwardedParsed   []string `json:"forwarded_parsed" bson:"forwarded_parsed,omitempty"`
	BlockType         string   `json:"blocktype" bson:"blocktype,omitempty"`
	countIPThreshold1 int      `json:"count_ip_threshold_1" bson:"count_ip_threshold_1,omitempty"`
	countIPThreshold2 int      `json:"count_ip_threshold_2" bson:"count_ip_threshold_2,omitempty"`
}

type IPdata struct {
	IsIpDataCenter    bool   `json:"ip_datacenter" bson:"ip_datacenter,omitempty"`
	IpVendor          string `json:"ip_vendor" bson:"ip_vendor,omitempty"`
	IpVendorUrl       string `json:"ip_vendor_url" bson:"ip_vendor_url,omitempty"`
	IsIpBot           bool   `json:"is_ip_bot" bson:"is_ip_bot,omitempty"`
	IpBotCountry      string `json:"bot_country" bson:"bot_country,omitempty"`
	IpBotCity         string `json:"bot_city" bson:"bot_city,omitempty"`
	IsIpGoodCrawler   bool   `json:"is_ip_good_crawler" bson:"is_ip_good_crawler,omitempty"`
	IpHostname        string `json:"ip_hostname" bson:"ip_hostname,omitempty"`
	IpCrawlerCategory string `json:"crawler_category" bson:"crawler_category,omitempty"`
	IsIpProxy         bool   `json:"is_proxy" bson:"is_proxy,omitempty"`
	IsIpTor           bool   `json:"is_tor" bson:"is_tor,omitempty"`
}

var (
	whiteListURLsmap = make(map[string]whiteListedURLs)
	blacklistIPmap   = make(map[string]blackListedIPs)
	whiteListIPmap   = make(map[string]whiteListedIPs)
)

type whiteListedURLs struct {
	Cid  string   `bson:"cid" json:"cid"`
	Urls []string `bson:"urls" json:"urls"`
}

type blackListIP struct {
	Ip     string `bson:"ip" json:"ip"`
	Ua     string `bson:"ua" json:"ua"`
	Reason string `bson:"reason" json:"reason"`
}

type blackListedIPs struct {
	BlackList []blackListIP `bson:"blacklist" json:"blacklist"`
	Cid       string        `bson:"cid" json:"cid"`
}

type whiteListIP struct {
	Ip string `bson:"ip" json:"ip"`
	Ua string `bson:"ua" json:"ua"`
}

type whiteListedIPs struct {
	WhiteList []whiteListIP `bson:"whitelist" json:"whitelist"`
	Cid       string        `bson:"cid" json:"cid"`
}

type runTimeBlockedIP struct {
	Ip     string `json:"ip"`
	Ua     string `json:"ua"`
	Reason string `json:"reason"`
	Action string `json:"action"`
}

var (
	master_ip   = regexp.MustCompile(`master_host:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)`)
	master_port = regexp.MustCompile(`master_port:([0-9]*)`)
)

var (
	//logFile = flag.String("log", "cerberus.log", "log file location")
	logLevel = flag.String("loglevel", "warning", "Log levels - debug, info, warning, all")
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

var MongoSession *mgo.Session

type UaData struct {
	UaClass                 string `json:"uaClassCode,omitempty",bson:"uaClassCode,omitempty"`
	Ua                      string `json:"ua,omitempty",bson:"ua,omitempty"`
	UaEngine                string `json:"uaEngine,omitempty",bson:"uaEngine,omitempty"`
	UaVersion               string `json:"uaVersion,omitempty",bson:"uaVersion,omitempty"`
	CrawlerCategory         string `json:"CrawlerCategory,omitempty",bson:"CrawlerCategory,omitempty"`
	UaFamily                string `json:"uaFamilyCode,omitempty",bson:"uaFamilyCode,omitempty"`
	CrawlerRespectRobotstxt string `json:"crawlerRespectRobotstxt,omitempty",bson:"crawlerRespectRobotstxt,omitempty"`
	UaFamilyHomepage        string `json:"uaFamilyHomepage,omitempty",bson:"uaFamilyHomepage,omitempty"`
	UaFamilyVendorCode      string `json:"uaFamilyVendorCode,omitempty",bson:"uaFamilyVendorCode,omitempty"`
	OsFamily                string `json:"osFamilyCode,omitempty",bson:"osFamilyCode,omitempty"`
	deviceClass             string `json:"deviceClassCode,omitempty",bson:"deviceClassCode,omitempty"`
}
