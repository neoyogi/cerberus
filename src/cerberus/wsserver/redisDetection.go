package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
)

func (is *ClientInfo) redisWorker() {
	defer is.defaultWG.Done()
	log := is.log.WithFields(logrus.Fields{"func": "redisWorker"})
	defer log.Debugln("Exit worker")
	for {
		select {
		case requestdata := <-is.redisCh:
			if requestdata.BlockType != "" {
				continue
			}
			func() {
				defer requestdata.checkDBWG.Done()
				defer log.Debugln("finished checking redis IP count")
				is.sendDataToDestRedis(requestdata)
				is.setblockIPcount(requestdata)
			}()

		case <-is.doneCh:
			return
		}
	}
}

func (is *ClientInfo) sendDataToDestRedis(requestData *RequestData) {
	log := is.log.WithFields(logrus.Fields{"func": "sendDataToDestRedis"})
	log.Debugln("Exit func")
	redisCon := is.pool.Get()
	defer redisCon.Close()

	//uid := string([]byte(fmt.Sprintf("%s", uuid.NewV4()))[0:8])
	key := "cid:" + is.cid + "||" + requestData.Ip + "||" + requestData.Ua + "||t1||" + requestData.Uid
	log.Debugln("data in dest redis:", key)
	_, err := redisCon.Do("setex", key, is.thresholdTime1, "")
	if err != nil {
		log.Debugln("Failed to insert the data into redis server, err: %s \n", err)
		is.once.Do(is.closeClient)
		return
	}

	//uid = string([]byte(fmt.Sprintf("%s", uuid.NewV4()))[0:8])
	log.Debugln("data in dest redis:", key)
	key = "cid:" + is.cid + "||" + requestData.Ip + "||" + requestData.Ua + "||t2||" + requestData.Uid
	_, err1 := redisCon.Do("setex", key, is.thresholdTime2, "")
	if err1 != nil {
		log.Warnln("Failed to insert the data into redis server, err: %s \n", err1)
		is.once.Do(is.closeClient)
		return
	}
}

func (is *ClientInfo) setblockIPcount(requestData *RequestData) {
	log := is.log.WithFields(logrus.Fields{"func": "setblockIPcount"})
	defer log.Debugln("exit func")
	redisCon := is.pool.Get()
	defer redisCon.Close()

	key := "cid:" + is.cid + "||" + requestData.Ip + "||" + requestData.Ua + "||t1||*"
	result2, err := redis.Strings(redisCon.Do("keys", key))
	if err != nil {
		log.Warnln("keys command on redis failed, err: ", err)
		is.once.Do(is.closeClient)
		return
	}
	countResult := len(result2)
	log.Debugln("keys * command Length :", countResult)
	if countResult >= is.thresholdCount1 {
		requestData.countIPThreshold1 = countResult
	}

	key = "cid:" + is.cid + "||" + requestData.Ip + "||" + requestData.Ua + "||t2||*"
	result3, err := redis.Strings(redisCon.Do("keys", key))
	if err != nil {
		log.Warnln("Failed to perform keys command on redis, err: ", err)
		is.once.Do(is.closeClient)
		return
	}
	countResult = len(result3)
	log.Debugln("keys * command Length :", countResult)
	if countResult >= is.thresholdCount2 {
		requestData.countIPThreshold2 = countResult
	}
}
