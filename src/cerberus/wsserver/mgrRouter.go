package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"strings"
	"sync"
	//"encoding/json"
	"github.com/pquerna/ffjson/ffjson"
	//"errors"
)

type Routes []Route

var routes = Routes{
	Route{
		"Detect",
		"GET",
		"/inspect",
		Inspect,
	},
	Route{
		"Detect",
		"GET",
		"/del/{cid:.*}/{sid:.*}",
		DelCidSid,
	},
	Route{
		"Detect",
		"GET",
		"/del/{cid:.*}",
		DelCid,
	},
	Route{
		"Detect",
		"GET",
		"/loglevel/{cid:.*}/{loglevel:.*}",
		LoglevelCid,
	},
	Route{
		"Detect",
		"GET",
		"/loglevel/{cid:.*}/{loglevel:.*}",
		LoglevelCidSid,
	},
}

func newRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		router.Methods(route.Method).Path(route.Pattern).Name(route.Name).Handler(handler)
	}
	return router
}

func Inspect(w http.ResponseWriter, r *http.Request) {
	type allclientInfos map[string][]string
	clientinfos := allclientInfos{}
	for client_id, _ := range clients {
		data := strings.Split(client_id, ":")
		sid := data[1]
		cid := data[0]
		clientinfos[cid] = append(clientinfos[cid], sid)
	}
	//data, _ := json.Marshal(clientinfos)
	data, _ := ffjson.Marshal(clientinfos)
	//fmt.Println(string(data))

	w.Write(data)
	//w.WriteHeader(200)
	return
}

func DelCidSid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cid := vars["cid"]
	sid := vars["sid"]
	clientinfo, ok := clients[cid+":"+sid]
	if !ok {
		return
	}
	clientinfo.once.Do(clientinfo.closeClient)
	//clientinfo.isdoneChClosed = true
	//close(clientinfo.doneCh)
	//w.WriteHeader(200)
	return
}

func DelCid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cid := vars["cid"]
	for key, _ := range clients {
		if strings.HasPrefix(key, cid) {
			clientinfo, ok := clients[key]
			if ok {
				clientinfo.once.Do(clientinfo.closeClient)
				//clientinfo.closeClient(errors.New("forced kill from manager API"))
			}
		}
	}
}

func LoglevelCid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cid := vars["cid"]
	loglevel := vars["loglevel"]
	for key, _ := range clients {
		if strings.HasPrefix(key, cid) {
			clientinfo, ok := clients[key]
			if ok {
				clientinfo.logger.Level = getLogLevel(loglevel)
			}
		}
	}
}

func LoglevelCidSid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cid := vars["cid"]
	sid := vars["sid"]
	loglevel := vars["loglevel"]
	clientinfo, ok := clients[cid+":"+sid]
	if ok {
		clientinfo.logger.Level = getLogLevel(loglevel)
	}
}

func mgrServer(mainWG *sync.WaitGroup) {
	defer mainWG.Done()
	router := newRouter()
	log.Fatal(http.ListenAndServe("localhost:"+"9000", router))
}
