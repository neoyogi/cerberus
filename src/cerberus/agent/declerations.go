package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	//"github.com/ua-parser/uap-go/uaparser"
	"net"
	"net/http"
	"regexp"
)

var log = logrus.New()

type RequestData struct {
	Ip              string `json:"ip"`
	Ua              string `json:"ua"`
	Url             string `json:"url"`
	//UaParsed        string `json:"uaparsed"`
	Uid             string
	Referrer        string   `json:"referrer"`
	Method          string   `json:"method"`
	ContentType     string   `json:"content_type"`
	Forwarded       string   `json:"forwarded"`
	Origin          string   `json:"origin"`
	Host            string   `json:"host"`
	Accept          string   `json:"accept"`
	ForwardedParsed []string `json:"forwarded_parsed"`
	BlockType       string   `json:"blocktype"`
}

type Result struct {
	Action string `json:"action"`
	Reason string `json:"reason"`
	Uid    string `json:"uid"`
}

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

var (
	customerData = make(chan RequestData, 1000)
	//parser       = uaparser.NewFromSaved()
)

var (
	cid      = flag.String("cid", "123456", "Customer ID provided by Cerberus")
	wsServer = flag.String("rserver", "localhost", "Remote Cerberus server IP address")
	wsPort   = flag.Int("rport", 3000, "Remote Cerberus server port")
	httpPort = flag.Int("port", 8080, "Agent port number")
	logLevel = flag.String("loglevel", "debug", "loglevels - debug, info, warning, all")
)

type whiteListedURLs struct {
	Cid  string   `json:"cid"`
	Urls []string `json:"urls"`
}

type blackListIP struct {
	Ip     string `json:"ip"`
	Ua     string `json:"ua"`
	Reason string `json:"reason"`
}

type blackListedIPs struct {
	BlackList []blackListIP `json:"blacklist"`
	Cid       string        `json:"cid"`
}

type whiteListIP struct {
	Ip string `json:"ip"`
	Ua string `json:"ua"`
}

type whiteListedIPs struct {
	WhiteList []whiteListIP `json:"whitelist"`
	Cid       string        `json:"cid"`
}

type runTimeBlockedIP struct {
	Ip       string `json:"ip"`
	UaParsed string `json:"ua"`
	Reason   string `json:"reason"`
	Action   string `json:"action"`
}

var (
	whiteListedUrldata = whiteListedURLs{}
	whiteListIPdata    = whiteListedIPs{}
	blackListIPdata    = blackListedIPs{}
	//runTimeBlockedIPUA = runTimeBlockedIP{}
	runTimeBlockedIPUAmap = make(map[runTimeBlockedIP]string)
)

//var uamap = make(map[string]parsedua)

//type parsedua struct {
//	uaparsed    string
//	updatedTime int64
//}

//type UAdata struct {
//	Ua       string `json:"ua"`
//	UaParsed string `json:"uaparsed"`
//}

type Routes []Route

var routes = Routes{
	Route{
		"Detect",
		"POST",
		"/detect",
		Detect,
	},
}
var doneCh = make(chan struct{})
var isdoneChClosed = false

var (
	IPregex     = regexp.MustCompile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)`)
	ipranges    = make(map[*net.IP]net.IP)
	IP127range1 = net.ParseIP("127.0.0.0")
	IP127range2 = net.ParseIP("127.255.255.255")
	IP192range1 = net.ParseIP("192.168.0.0")
	IP192range2 = net.ParseIP("192.168.255.255")
	IP10range1  = net.ParseIP("10.0.0.0")
	IP10range2  = net.ParseIP("10.255.255.255")
	IP172range1 = net.ParseIP("172.16.0.0")
	IP172range2 = net.ParseIP("172.31.255.255")
)
