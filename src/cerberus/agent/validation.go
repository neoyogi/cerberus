package main

import (
	"errors"
	"github.com/Sirupsen/logrus"
	"os"
)

func getBlockStatus(requestData RequestData) Result {
	log := log.WithFields(logrus.Fields{"func": "getBlockStatus"})
	// Validation to check for config first (blip, wlip) and then dynamically generated block IP list, if positive respond with BLOCK and send the request data with appropriate tag to WSS for logging.
	// if nothing can be decided (negative), respond with OK and send the request to WSS for further investigation

	// 1. Check with BlackListed IP List
	for _, blip := range blackListIPdata.BlackList {
		if requestData.Ip == blip.Ip {
			log.Info("Allowing Blacklisted IP: ", blip.Ip)
			return Result{Action: "BLOCK", Reason: "Denied"}
		}
	}

	// 2. Check with Whitelisted IP List
	for _, wlip := range whiteListIPdata.WhiteList {
		if requestData.Ip == wlip.Ip {
			log.Info("Allowing Whitelisted IP: ", wlip.Ip)
			return Result{Action: "OK", Reason: "Allowed"}
		}
	}

	// 3. Check with dynamic Blocked IP List
	for blocked, _ := range runTimeBlockedIPUAmap {
		if requestData.Ip == blocked.Ip {
			if requestData.Ua== blocked.UaParsed {
				log.Info("Blocking Runtime blocked IP: ", blocked.Ip)
				requestData.BlockType = "IPCOUNTBLOCK"
				go send(requestData)
				return Result{Action: "BLOCK", Reason: "Denied (Run time)"}
			}
		}
	}

	// 4. Send the request to WSS for investigation
	go send(requestData)
	return Result{Action: "OK", Reason: "Pending"}
}

func send(requestData RequestData) {
	if len(customerData) != 0 {
		log.Debugln("length:", len(customerData))
	}
	if len(customerData) != cap(customerData) {
		select {
		case customerData <- requestData:
		case <-doneCh:
			return
		}
	} else {
		if isdoneChClosed {
			os.Exit(-1)
		}
		log.Warnln("queue is full!!, panic!")
		closechannel(errors.New("queue is full"))
		os.Exit(-1)
	}
}
