package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/rifflock/lfshook"
	"net/http"
	"strconv"
	"sync"
	"time"
)

var wg sync.WaitGroup

func init() {
	ipranges[&IP127range1] = IP127range2
	ipranges[&IP10range1] = IP10range2
	ipranges[&IP192range1] = IP192range2
	ipranges[&IP172range1] = IP172range2
}

func main() {
	log.Formatter = new(logrus.TextFormatter) // default
	log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{logrus.DebugLevel: "cerberus-agent.log", logrus.InfoLevel: "cerberus-agent.log", logrus.WarnLevel: "cerberus-agent.log", logrus.ErrorLevel: "cerberus-agent.log"}))
	switch loglevel := *logLevel; loglevel {
	case "debug":
		log.Level = logrus.DebugLevel
	case "info":
		log.Level = logrus.InfoLevel
	case "warning":
		log.Level = logrus.WarnLevel
	}
	wg.Add(1)
	go retryws()
	wg.Add(1)
	go server()
	wg.Wait()
}

func server() {
	defer wg.Done()
	router := NewRouter()
	srv := &http.Server{
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
		Addr:         "localhost:" + strconv.Itoa(*httpPort),
		Handler:      router,
	}
	//log.Fatal(http.ListenAndServe("localhost:"+strconv.Itoa(*httpPort), router))
	log.Fatal(srv.ListenAndServe())
}
