package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/pquerna/ffjson/ffjson"
	"strings"
	"time"
)

func processData(data []byte) {
	log := log.WithFields(logrus.Fields{"func": "processData"})
	strdata := string(data)
	prefix := getPrefix(strdata)
	jsonData := []byte(strings.TrimPrefix(strdata, prefix))
	switch prefix {
	case "WHITELISTURLS:":
		err := ffjson.Unmarshal(jsonData, &whiteListedUrldata)
		if err != nil {
			log.Warnln("failed to unmarshal the data for whiltelistUrls, err: %s", err)
		}
		//log.Println(whiteListedUrldata)
	case "WHITELISTIPS:":
		err := ffjson.Unmarshal(jsonData, &whiteListIPdata)
		if err != nil {
			log.Warnln("failed to unmarshal the data for whiltelistIps, err: %s", err)
		}
		//log.Println(whiteListIPdata)
	case "BLACKLISTIPS:":
		err := ffjson.Unmarshal(jsonData, &blackListIPdata)
		if err != nil {
			log.Warnln("failed to unmarshal the data for BlacklistIPs, err: %s", err)
		}
		//log.Println(blackListIPdata)
	case "IPCOUNTBLOCK:":
		runTimeBlockedIPUA := runTimeBlockedIP{}
		err := ffjson.Unmarshal(jsonData, &runTimeBlockedIPUA)
		if err != nil {
			log.Warnln("failed to unmarshal the data for runtimeBlockedIPUA, err: %s", err)
		}
		runTimeBlockedIPUAmap[runTimeBlockedIPUA] = time.Now().String()
		log.Warnln("blocking IP and UA: ", runTimeBlockedIPUAmap)

	case "IPCOUNTUNBLOCK:":
		runTimeBlockedIPUA := runTimeBlockedIP{}
		err := ffjson.Unmarshal(jsonData, &runTimeBlockedIPUA)
		if err != nil {
			log.Warnln("failed to unmarshal the data for IPCOUNTUNBLOCK, err: %s", err)
		}
		delete(runTimeBlockedIPUAmap, runTimeBlockedIPUA)
		log.Warnln("Unblocking IP and UA", runTimeBlockedIPUAmap)

	//case "UAUPDATE:":
	//	uadata := UAdata{}
	//	err := ffjson.Unmarshal(jsonData, &uadata)
	//	if err != nil {
	//		log.Warnln("failed to unmarshal the data for UAUPDATE, err: %s", err)
	//	}
	//	//uamap[uadata.Ua]
	//	if _, ok := uamap[uadata.Ua]; !ok {
	//		uamap[uadata.Ua] = parsedua{uaparsed: uadata.UaParsed, updatedTime: time.Now().Unix() + 120}
	//	}

	default:
		log.Warnln("Unknown message type, message: ", prefix)
	}
}

func getPrefix(message string) string {
	indexPosition := strings.Index(message, ":")
	if indexPosition == -1 {
		log.Warnln("failed to find a prefix in the message: %s", message)
	}
	prefix := message[:indexPosition+1]
	return prefix
}
