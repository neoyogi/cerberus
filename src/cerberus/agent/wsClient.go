package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/cenk/backoff"
	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
	"mimeparse"
	"net"
	"net/http"
	"strconv"
	"sync"
	"time"
)

func closechannel(err error) {
	log := log.WithFields(logrus.Fields{"func": "closechannel"})
	log.Warnln("closing channel")
	if !isdoneChClosed {
		isdoneChClosed = true
		close(doneCh)
	}
}

func openChannel() {
	log := log.WithFields(logrus.Fields{"func": "openChannel"})
	log.Debugln("openining channel")
	if isdoneChClosed {
		isdoneChClosed = false
		doneCh = make(chan struct{})
	}
}

func retryws() error {
	log := log.WithFields(logrus.Fields{"func": "retryws"})
	defer log.Errorln("failed after many attempts to connect to the websocket server")
	wsconnect := func() error {
		log := log.WithFields(logrus.Fields{"func": "wsconnect"})
		err := wsConnect()
		log.Warnln("error in wsconnect:", err)
		return err
	}
	err := backoff.Retry(wsconnect, backoff.NewConstantBackOff(time.Second))
	if err != nil {
		log.Panicf("Failed to connect to the Cerberus Labs network")
		return err
	}
	return nil
}

func wsConnect() error {
	openChannel()
	defer closechannel(errors.New("WS connection ended"))
	wsWG := new(sync.WaitGroup)
	log := log.WithFields(logrus.Fields{"func": "wsConnect"})
	defer log.Warnln("ws connection ended")
	flag.Parse()
	// TODO Flag to set  customer id
	header := http.Header{}
	header.Set("Origin", *cid)
	sid := fmt.Sprintf("%s", uuid.NewV4())
	header.Set("_bhsid", sid)
	log.Infoln("connection: ", sid)
	connection, _, err := websocket.DefaultDialer.Dial("ws://"+*wsServer+":"+strconv.Itoa(*wsPort)+"/ws", header)
	if err != nil {
		log.Warnln("Dial error: %s \n", err)
		return errors.New("failed to connect")
	}
	defer connection.Close()
	wsWG.Add(1)
	go func() {
		log := log.WithFields(logrus.Fields{"func": "wsConnectSubModule1"})
		defer log.Debugln("done")
		defer wsWG.Done()
		for {
			_, message, err := connection.ReadMessage()
			if err != nil {
				log.Warnln("read err: %s\n", err)
				closechannel(err)
				return
			}
			log.Debugln("recv %s \n", string(message))
			processData(message)
		}
	}()
	wsWG.Add(1)
	go func() {
		log := log.WithFields(logrus.Fields{"func": "wsConnectSubModule2"})
		defer log.Debugln("done")
		defer wsWG.Done()
		for {
			select {
			case clData := <-customerData:
				//if val, ok := uamap[clData.Ua]; !ok {
				//	clData.UaParsed = parser.Parse(clData.Ua).UserAgent.ToString()
				//	uamap[clData.Ua] = parsedua{uaparsed: clData.UaParsed, updatedTime: time.Now().Unix() + 600}
				//} else {
				//	clData.UaParsed = val.uaparsed
				//}
				if clData.Ip == "" && clData.Forwarded != "" {
					parsedIPs := make([]string, 0)
					listOfIPs := mimeparse.ParseList(clData.Forwarded)

				continuehere:
					for _, ip := range listOfIPs {
						Forwardedipmatch := IPregex.FindStringSubmatch(ip)
						forwardedip := net.ParseIP(Forwardedipmatch[0])
						if forwardedip.To4() == nil {
							continue
						}
						for startIP, endIP := range ipranges {
							if bytes.Compare(forwardedip, *startIP) >= 0 && bytes.Compare(forwardedip, endIP) <= 0 {
								continue continuehere
							}
						}
						parsedIPs = append(parsedIPs, forwardedip.String())
					}
					clData.ForwardedParsed = parsedIPs
					if len(clData.ForwardedParsed) > 0 {
						clData.Ip = clData.ForwardedParsed[0]
					}
				}

				//clData.Uid = uuid.NewV4().String()
				err := connection.WriteJSON(clData)
				if err != nil {
					log.Warnln("Unable to send the json data %s \n", err)
					closechannel(err)
					return
				}
			case <-doneCh:
				return
			}
		}
	}()
	//wsWG.Add(1)
	//go parseUA(wsWG, doneCh)
	wsWG.Wait()
	return errors.New("WS connection ended")
}
