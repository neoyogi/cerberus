package main

import (
	"encoding/json"
	"github.com/Sirupsen/logrus"
	"github.com/pquerna/ffjson/ffjson"
	"github.com/satori/go.uuid"
	"io"
	"io/ioutil"
	"net/http"
)

func Detect(w http.ResponseWriter, r *http.Request) {
	log := log.WithFields(logrus.Fields{"func": "Detect"})
	var requestData RequestData
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Warnln("Failed to read from http client request: %s", err)
	}
	defer r.Body.Close()

	if err := ffjson.Unmarshal(body, &requestData); err != nil {
		w.WriteHeader(400) // bad data
		return
	}
	uid := uuid.NewV4().String()
	requestData.Uid = uid

	result := getBlockStatus(requestData)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		log.Warnln("Failed to encode the json in http response, %s", err)
	}
}
