package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"net/http"
	"sync"
)

var wg sync.WaitGroup
var log = logrus.New()
var logLevel = flag.String("loglevel", "debug", "loglevels - debug, info, warning, all")
var httpPort = flag.Int("port", 8080, "js server port number")

var (
	localRedisPool     *redis.Pool
	detectionRedisPool = make(map[string]*redis.Pool)
	//masterRedisPool    = make(map[string]*redis.Pool)
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Detect",
		"POST",
		"/jschk",
		JsCheck,
	},
}

type JsData struct {
	FingerPrint string `schema:"fp"` // custom name
	Domain      string `schema:"cd"` // custom name
	Referer     string `schema:"refer"`
	Uri         string `schema:"uri"`
	Id          string `schema:"id"`
	//Admin bool   `schema:"-"`     // this field is never set
}
