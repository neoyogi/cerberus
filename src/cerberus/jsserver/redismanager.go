package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"strconv"
)

func newPool(host string, port int, timeout int, maxactive int) *redis.Pool {
	log := log.WithFields(logrus.Fields{"func": "newPool", "host": host, "port": port, "timeout": timeout, "maxactive": maxactive})
	log.Debugln("Creating new pool")
	return &redis.Pool{
		MaxIdle:   timeout,
		MaxActive: maxactive, // max number of connections
		Dial: func() (redis.Conn, error) {
			log.Debugln("trying to dial")
			c, err := redis.Dial("tcp", host+":"+strconv.Itoa(port))
			if err != nil {
				log.Warnln("Failed to dial: ", err.Error())
			}
			log.Debugln("dial successful")
			return c, err
		},
	}
}

func hgetRediskeyInt(key string, field string) int {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.Int(redisCon.Do("hget", key, field))
	if err != nil {
		log.Warnln("failed to perform hget operation on redis", err)
	}
	return result
}

func hgetRediskeyString(key string, field string) string {
	redisCon := localRedisPool.Get()
	defer redisCon.Close()
	result, err := redis.String(redisCon.Do("hget", key, field))
	if err != nil {
		log.Warnln("failed to perform hget operation on redis", err)
	}
	return result
}
