package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/schema"
	"net/http"
	"strings"
)

func JsCheck(w http.ResponseWriter, r *http.Request) {
	log := log.WithFields(logrus.Fields{"func": "JsCheck"})

	if err := r.ParseForm(); err != nil {
		log.Warnf("Failed to parse the form: %s", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	jsData := new(JsData)
	decoder := schema.NewDecoder()

	if err := decoder.Decode(jsData, r.Form); err != nil {
		log.Warnf("Failed to decode the form values: %s", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	origin := r.Header.Get("Origin")
	if !strings.Contains(origin, jsData.Domain) {
		log.Warnf("Origin %s does not match domain %s", origin, jsData.Domain)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	go updateRedis(jsData)
	w.WriteHeader(http.StatusOK)
}

func updateRedis(jsData *JsData) {
	log := log.WithFields(logrus.Fields{"func": "updateRedis"})

	var cid string
	var err error
	if cid, err = getCid(jsData); err != nil {
		log.Warnf("Unable to get the cid for domain %s, err: %s", jsData.Domain, err)
		return
	}

	if _, ok := detectionRedisPool[cid]; !ok {
		log.Debugln("DetectionRedisPool map doesn't contain the pool, creating new pool", detectionRedisPool)
		detectionRedisPool[cid] = newPool(hgetRediskeyString("cid:"+cid, "destredis_ip"), hgetRediskeyInt("cid:"+cid, "destredis_port"), 120, 200)
	} else {
		log.Debugln("using the existing detection redis pool", detectionRedisPool[cid])
	}

	/*ids := strings.Split(jsData.Id, "-")
	key := cid + ":" + ids[0] + ":js:" + strings.Join(ids[1:5], "")*/

	detRedisCon := detectionRedisPool[cid].Get()
	defer detRedisCon.Close()

	//var getScript = redis.NewScript(0, `return redis.call('del', unpack(redis.call('keys', ARGV[1])))`)
	//result, err := getScript.Do(detRedisCon, "*||js||" + jsData.Id)

	keys, err := redis.Strings(detRedisCon.Do("keys", "*||js||"+jsData.Id))
	if err != nil || len(keys) == 0 {
		log.Warnln("Failed to find the key in redis server, err: ", err)
		return
	}

	result, err := redis.Int(detRedisCon.Do("del", keys[0]))
	if err != nil || result != 1 {
		log.Warnln("Failed to delete the data in redis server, err: ", err)
		return
	}
}

func getCid(jsData *JsData) (string, error) {
	log := log.WithFields(logrus.Fields{"func": "getCid"})
	redisCon := localRedisPool.Get()
	defer redisCon.Close()

	cid, err := redis.String(redisCon.Do("get", jsData.Domain))
	if err != nil {
		log.Warnf("Domain not found %s, err: %s", jsData.Domain, err)
		return "", err
	}
	return cid, nil
}
