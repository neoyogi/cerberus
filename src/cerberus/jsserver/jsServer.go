package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"github.com/rifflock/lfshook"
	"net/http"
	"strconv"
	"time"
)

func init() {
	flag.Parse()

	log.Formatter = new(logrus.TextFormatter) // default
	log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{logrus.DebugLevel: "cerberus-js.log", logrus.InfoLevel: "cerberus-js.log", logrus.WarnLevel: "cerberus-js.log", logrus.ErrorLevel: "cerberus-js.log"}))
	switch loglevel := *logLevel; loglevel {
	case "debug":
		log.Level = logrus.DebugLevel
	case "info":
		log.Level = logrus.InfoLevel
	case "warning":
		log.Level = logrus.WarnLevel
	}
}

func main() {
	defer log.Warnln("Websocket Server is down!")
	localRedisPool = newPool("127.0.0.1", 6379, 120, 0)

	wg.Add(1)
	go server()
	wg.Wait()
}

func server() {
	defer wg.Done()
	router := NewRouter()
	srv := &http.Server{
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
		Addr:         "localhost:" + strconv.Itoa(*httpPort),
		Handler:      router,
	}
	log.Fatal(srv.ListenAndServe())
}
